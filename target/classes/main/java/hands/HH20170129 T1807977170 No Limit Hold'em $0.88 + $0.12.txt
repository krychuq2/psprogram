﻿PokerStars Hand #165374892233: Tournament #1807977170, $0.44+$0.44+$0.12 USD Hold'em No Limit - Level I (25/50) - 2017/01/29 22:55:56 CET [2017/01/29 16:55:56 ET]
Table '1807977170 2' 6-max Seat #1 is the button
Seat 1: krychuq83th (500 in chips) 
Seat 2: Stars-ID1 (500 in chips) 
Seat 3: horatziu_1986 (500 in chips) 
Seat 4: markoes33 (500 in chips) 
Seat 5: dlinlyolik (500 in chips) 
Seat 6: sorry.(1533) (500 in chips) 
krychuq83th: posts the ante 10
Stars-ID1: posts the ante 10
horatziu_1986: posts the ante 10
markoes33: posts the ante 10
dlinlyolik: posts the ante 10
sorry.(1533): posts the ante 10
Stars-ID1: posts small blind 25
horatziu_1986: posts big blind 50
*** HOLE CARDS ***
Dealt to krychuq83th [7h 4s]
markoes33: folds 
dlinlyolik: raises 440 to 490 and is all-in
sorry.(1533): folds 
krychuq83th: folds 
Stars-ID1: folds 
horatziu_1986: calls 440 and is all-in
*** FLOP *** [Jh 5d Td]
*** TURN *** [Jh 5d Td] [Qd]
*** RIVER *** [Jh 5d Td Qd] [9c]
*** SHOW DOWN ***
horatziu_1986: shows [Ks Kh] (a straight, Nine to King)
dlinlyolik: shows [4c 4h] (a pair of Fours)
horatziu_1986 collected 1065 from pot
horatziu_1986 wins the $0.44 bounty for eliminating dlinlyolik
dlinlyolik finished the tournament in 34th place
*** SUMMARY ***
Total pot 1065 | Rake 0 
Board [Jh 5d Td Qd 9c]
Seat 1: krychuq83th (button) folded before Flop (didn't bet)
Seat 2: Stars-ID1 (small blind) folded before Flop
Seat 3: horatziu_1986 (big blind) showed [Ks Kh] and won (1065) with a straight, Nine to King
Seat 4: markoes33 folded before Flop (didn't bet)
Seat 5: dlinlyolik showed [4c 4h] and lost with a pair of Fours
Seat 6: sorry.(1533) folded before Flop (didn't bet)



PokerStars Hand #165374910040: Tournament #1807977170, $0.44+$0.44+$0.12 USD Hold'em No Limit - Level I (25/50) - 2017/01/29 22:56:20 CET [2017/01/29 16:56:20 ET]
Table '1807977170 2' 6-max Seat #2 is the button
Seat 1: krychuq83th (490 in chips) 
Seat 2: Stars-ID1 (465 in chips) 
Seat 3: horatziu_1986 (1065 in chips) 
Seat 4: markoes33 (490 in chips) 
Seat 6: sorry.(1533) (490 in chips) 
krychuq83th: posts the ante 10
Stars-ID1: posts the ante 10
horatziu_1986: posts the ante 10
markoes33: posts the ante 10
sorry.(1533): posts the ante 10
horatziu_1986: posts small blind 25
markoes33: posts big blind 50
*** HOLE CARDS ***
Dealt to krychuq83th [Js As]
sorry.(1533): folds 
krychuq83th: raises 430 to 480 and is all-in
Stars-ID1: folds 
horatziu_1986: calls 455
markoes33: folds 
*** FLOP *** [7d Kc 4c]
*** TURN *** [7d Kc 4c] [Qd]
*** RIVER *** [7d Kc 4c Qd] [5h]
*** SHOW DOWN ***
horatziu_1986: shows [5d 6s] (a pair of Fives)
krychuq83th: shows [Js As] (high card Ace)
horatziu_1986 collected 1060 from pot
horatziu_1986 wins the $0.44 bounty for eliminating krychuq83th
krychuq83th finished the tournament in 30th place
*** SUMMARY ***
Total pot 1060 | Rake 0 
Board [7d Kc 4c Qd 5h]
Seat 1: krychuq83th showed [Js As] and lost with high card Ace
Seat 2: Stars-ID1 (button) folded before Flop (didn't bet)
Seat 3: horatziu_1986 (small blind) showed [5d 6s] and won (1060) with a pair of Fives
Seat 4: markoes33 (big blind) folded before Flop
Seat 6: sorry.(1533) folded before Flop (didn't bet)



