﻿PokerStars Hand #165668705362: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:56:23 CET [2017/02/04 10:56:23 ET]
Table '1814034223 1' 3-max Seat #1 is the button
Seat 1: YouReMyPeach (500 in chips) 
Seat 2: krychuq83th (500 in chips) 
Seat 3: paam66 (500 in chips) 
krychuq83th: posts small blind 10
paam66: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [7c 2s]
YouReMyPeach: raises 20 to 40
krychuq83th: folds 
paam66: calls 20
*** FLOP *** [Ad Js 3h]
paam66: bets 80
YouReMyPeach: raises 80 to 160
paam66: calls 80
*** TURN *** [Ad Js 3h] [4s]
paam66: bets 300 and is all-in
YouReMyPeach: calls 300 and is all-in
*** RIVER *** [Ad Js 3h 4s] [5s]
*** SHOW DOWN ***
paam66: shows [Ac 4c] (two pair, Aces and Fours)
YouReMyPeach: shows [As 8d] (a pair of Aces)
paam66 collected 1010 from pot
YouReMyPeach finished the tournament in 3rd place
*** SUMMARY ***
Total pot 1010 | Rake 0 
Board [Ad Js 3h 4s 5s]
Seat 1: YouReMyPeach (button) showed [As 8d] and lost with a pair of Aces
Seat 2: krychuq83th (small blind) folded before Flop
Seat 3: paam66 (big blind) showed [Ac 4c] and won (1010) with two pair, Aces and Fours



PokerStars Hand #165668739552: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:57:13 CET [2017/02/04 10:57:13 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (490 in chips) 
Seat 3: paam66 (1010 in chips) 
krychuq83th: posts small blind 10
paam66: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [5s 2h]
krychuq83th: folds 
Uncalled bet (10) returned to paam66
paam66 collected 20 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 20 | Rake 0 
Seat 2: krychuq83th (button) (small blind) folded before Flop
Seat 3: paam66 (big blind) collected (20)



PokerStars Hand #165668742155: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:57:16 CET [2017/02/04 10:57:16 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (480 in chips) 
Seat 3: paam66 (1020 in chips) 
paam66: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Ks 7s]
paam66: raises 20 to 40
krychuq83th: calls 20
*** FLOP *** [Qc 7h Js]
krychuq83th: checks 
paam66: bets 20
krychuq83th: calls 20
*** TURN *** [Qc 7h Js] [Kc]
krychuq83th: bets 60
paam66: calls 60
*** RIVER *** [Qc 7h Js Kc] [3d]
krychuq83th: bets 120
paam66: folds 
Uncalled bet (120) returned to krychuq83th
krychuq83th collected 240 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 240 | Rake 0 
Board [Qc 7h Js Kc 3d]
Seat 2: krychuq83th (big blind) collected (240)
Seat 3: paam66 (button) (small blind) folded on the River



PokerStars Hand #165668766365: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:57:52 CET [2017/02/04 10:57:52 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (600 in chips) 
Seat 3: paam66 (900 in chips) 
krychuq83th: posts small blind 10
paam66: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [8h 2d]
krychuq83th: calls 10
paam66: raises 20 to 40
krychuq83th: folds 
Uncalled bet (20) returned to paam66
paam66 collected 40 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 40 | Rake 0 
Seat 2: krychuq83th (button) (small blind) folded before Flop
Seat 3: paam66 (big blind) collected (40)



PokerStars Hand #165668785480: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:58:19 CET [2017/02/04 10:58:19 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (580 in chips) 
Seat 3: paam66 (920 in chips) 
paam66: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [6h Ad]
paam66: calls 10
krychuq83th: checks 
*** FLOP *** [2d Td Tc]
krychuq83th: checks 
paam66: checks 
*** TURN *** [2d Td Tc] [Kh]
krychuq83th: checks 
paam66: checks 
*** RIVER *** [2d Td Tc Kh] [Jh]
krychuq83th: checks 
paam66: checks 
*** SHOW DOWN ***
krychuq83th: shows [6h Ad] (a pair of Tens)
paam66: mucks hand 
krychuq83th collected 40 from pot
*** SUMMARY ***
Total pot 40 | Rake 0 
Board [2d Td Tc Kh Jh]
Seat 2: krychuq83th (big blind) showed [6h Ad] and won (40) with a pair of Tens
Seat 3: paam66 (button) (small blind) mucked [4c 5h]



PokerStars Hand #165668809369: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:58:54 CET [2017/02/04 10:58:54 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (600 in chips) 
Seat 3: paam66 (900 in chips) 
krychuq83th: posts small blind 10
paam66: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [6d 7s]
krychuq83th: folds 
Uncalled bet (10) returned to paam66
paam66 collected 20 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 20 | Rake 0 
Seat 2: krychuq83th (button) (small blind) folded before Flop
Seat 3: paam66 (big blind) collected (20)



PokerStars Hand #165668812340: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:58:58 CET [2017/02/04 10:58:58 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (590 in chips) 
Seat 3: paam66 (910 in chips) 
paam66: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [3s 6c]
paam66: raises 20 to 40
krychuq83th: folds 
Uncalled bet (20) returned to paam66
paam66 collected 40 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 40 | Rake 0 
Seat 2: krychuq83th (big blind) folded before Flop
Seat 3: paam66 (button) (small blind) collected (40)



PokerStars Hand #165668819877: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:59:09 CET [2017/02/04 10:59:09 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (570 in chips) 
Seat 3: paam66 (930 in chips) 
krychuq83th: posts small blind 10
paam66: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Jc Js]
krychuq83th: raises 20 to 40
paam66: folds 
Uncalled bet (20) returned to krychuq83th
krychuq83th collected 40 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 40 | Rake 0 
Seat 2: krychuq83th (button) (small blind) collected (40)
Seat 3: paam66 (big blind) folded before Flop



PokerStars Hand #165668822879: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:59:14 CET [2017/02/04 10:59:14 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (590 in chips) 
Seat 3: paam66 (910 in chips) 
paam66: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [9d 4d]
paam66: calls 10
krychuq83th: checks 
*** FLOP *** [2c 8s 2d]
krychuq83th: bets 20
paam66: calls 20
*** TURN *** [2c 8s 2d] [3c]
krychuq83th: bets 60
paam66: calls 60
*** RIVER *** [2c 8s 2d 3c] [Qc]
krychuq83th: checks 
paam66: bets 200
krychuq83th: folds 
Uncalled bet (200) returned to paam66
paam66 collected 200 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 200 | Rake 0 
Board [2c 8s 2d 3c Qc]
Seat 2: krychuq83th (big blind) folded on the River
Seat 3: paam66 (button) (small blind) collected (200)



PokerStars Hand #165668841995: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 16:59:41 CET [2017/02/04 10:59:41 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (490 in chips) 
Seat 3: paam66 (1010 in chips) 
krychuq83th: posts small blind 15
paam66: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [7h Ac]
krychuq83th: raises 90 to 120
paam66: folds 
Uncalled bet (90) returned to krychuq83th
krychuq83th collected 60 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 60 | Rake 0 
Seat 2: krychuq83th (button) (small blind) collected (60)
Seat 3: paam66 (big blind) folded before Flop



PokerStars Hand #165668846864: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 16:59:48 CET [2017/02/04 10:59:48 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (520 in chips) 
Seat 3: paam66 (980 in chips) 
paam66: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [7d Ah]
paam66: raises 30 to 60
krychuq83th: calls 30
*** FLOP *** [6s Th 8d]
krychuq83th: checks 
paam66: bets 30
krychuq83th: folds 
Uncalled bet (30) returned to paam66
paam66 collected 120 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 120 | Rake 0 
Board [6s Th 8d]
Seat 2: krychuq83th (big blind) folded on the Flop
Seat 3: paam66 (button) (small blind) collected (120)



PokerStars Hand #165668855472: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 17:00:01 CET [2017/02/04 11:00:01 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (460 in chips) 
Seat 3: paam66 (1040 in chips) 
krychuq83th: posts small blind 15
paam66: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [5s 9h]
krychuq83th: folds 
Uncalled bet (15) returned to paam66
paam66 collected 30 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 30 | Rake 0 
Seat 2: krychuq83th (button) (small blind) folded before Flop
Seat 3: paam66 (big blind) collected (30)



PokerStars Hand #165668858699: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 17:00:05 CET [2017/02/04 11:00:05 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (445 in chips) 
Seat 3: paam66 (1055 in chips) 
paam66: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [9h 7c]
paam66: raises 30 to 60
krychuq83th: folds 
Uncalled bet (30) returned to paam66
paam66 collected 60 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 60 | Rake 0 
Seat 2: krychuq83th (big blind) folded before Flop
Seat 3: paam66 (button) (small blind) collected (60)



PokerStars Hand #165668862505: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 17:00:11 CET [2017/02/04 11:00:11 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (415 in chips) 
Seat 3: paam66 (1085 in chips) 
krychuq83th: posts small blind 15
paam66: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [4d 4s]
krychuq83th: raises 60 to 90
paam66: calls 60
*** FLOP *** [9s Qs Js]
paam66: checks 
krychuq83th: checks 
*** TURN *** [9s Qs Js] [Td]
paam66: checks 
krychuq83th: checks 
*** RIVER *** [9s Qs Js Td] [Th]
paam66: checks 
krychuq83th: checks 
*** SHOW DOWN ***
paam66: shows [5d 2d] (a pair of Tens)
krychuq83th: shows [4d 4s] (two pair, Tens and Fours)
krychuq83th collected 180 from pot
*** SUMMARY ***
Total pot 180 | Rake 0 
Board [9s Qs Js Td Th]
Seat 2: krychuq83th (button) (small blind) showed [4d 4s] and won (180) with two pair, Tens and Fours
Seat 3: paam66 (big blind) showed [5d 2d] and lost with a pair of Tens



PokerStars Hand #165668874979: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 17:00:27 CET [2017/02/04 11:00:27 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (505 in chips) 
Seat 3: paam66 (995 in chips) 
paam66: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [4c Jc]
paam66: calls 15
krychuq83th: checks 
*** FLOP *** [Kc 6s Ts]
krychuq83th: checks 
paam66: bets 60
krychuq83th: folds 
Uncalled bet (60) returned to paam66
paam66 collected 60 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 60 | Rake 0 
Board [Kc 6s Ts]
Seat 2: krychuq83th (big blind) folded on the Flop
Seat 3: paam66 (button) (small blind) collected (60)



PokerStars Hand #165668884733: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 17:00:41 CET [2017/02/04 11:00:41 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (475 in chips) 
Seat 3: paam66 (1025 in chips) 
krychuq83th: posts small blind 15
paam66: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [2h Jh]
krychuq83th: folds 
Uncalled bet (15) returned to paam66
paam66 collected 30 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 30 | Rake 0 
Seat 2: krychuq83th (button) (small blind) folded before Flop
Seat 3: paam66 (big blind) collected (30)



PokerStars Hand #165668887467: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 17:00:45 CET [2017/02/04 11:00:45 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (460 in chips) 
Seat 3: paam66 (1040 in chips) 
paam66: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Qh Qc]
paam66: calls 15
krychuq83th: raises 60 to 90
paam66: calls 60
*** FLOP *** [3h Tc 3d]
krychuq83th: bets 120
paam66: folds 
Uncalled bet (120) returned to krychuq83th
krychuq83th collected 180 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 180 | Rake 0 
Board [3h Tc 3d]
Seat 2: krychuq83th (big blind) collected (180)
Seat 3: paam66 (button) (small blind) folded on the Flop



PokerStars Hand #165668895705: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 17:00:56 CET [2017/02/04 11:00:56 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (550 in chips) 
Seat 3: paam66 (950 in chips) 
krychuq83th: posts small blind 15
paam66: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Kc 8c]
krychuq83th: raises 30 to 60
paam66: raises 890 to 950 and is all-in
krychuq83th: folds [Kc]
Uncalled bet (890) returned to paam66
paam66 collected 120 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 120 | Rake 0 
Seat 2: krychuq83th (button) (small blind) folded before Flop
Seat 3: paam66 (big blind) collected (120)



PokerStars Hand #165668911174: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 17:01:16 CET [2017/02/04 11:01:16 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (490 in chips) 
Seat 3: paam66 (1010 in chips) 
paam66: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Qh Kc]
paam66: calls 15
krychuq83th: raises 60 to 90
paam66: calls 60
*** FLOP *** [9d 2d 3s]
krychuq83th: bets 90
paam66: raises 830 to 920 and is all-in
krychuq83th: folds [Qh Kc]
Uncalled bet (830) returned to paam66
paam66 collected 360 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 360 | Rake 0 
Board [9d 2d 3s]
Seat 2: krychuq83th (big blind) folded on the Flop
Seat 3: paam66 (button) (small blind) collected (360)



PokerStars Hand #165668946731: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 17:02:02 CET [2017/02/04 11:02:02 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (310 in chips) 
Seat 3: paam66 (1190 in chips) 
krychuq83th: posts small blind 15
paam66: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [8c Tc]
krychuq83th: calls 15
paam66: checks 
*** FLOP *** [3d 8d Kd]
paam66: checks 
krychuq83th: checks 
*** TURN *** [3d 8d Kd] [5s]
paam66: bets 1160 and is all-in
krychuq83th: folds 
Uncalled bet (1160) returned to paam66
paam66 collected 60 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 60 | Rake 0 
Board [3d 8d Kd 5s]
Seat 2: krychuq83th (button) (small blind) folded on the Turn
Seat 3: paam66 (big blind) collected (60)



PokerStars Hand #165668962843: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 17:02:22 CET [2017/02/04 11:02:22 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (280 in chips) 
Seat 3: paam66 (1220 in chips) 
paam66: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [5s 9d]
paam66: calls 15
krychuq83th: checks 
*** FLOP *** [Ad 8d 9s]
krychuq83th: bets 250 and is all-in
paam66: calls 250
*** TURN *** [Ad 8d 9s] [8c]
*** RIVER *** [Ad 8d 9s 8c] [2s]
*** SHOW DOWN ***
krychuq83th: shows [5s 9d] (two pair, Nines and Eights)
paam66: shows [Jd Kd] (a pair of Eights)
krychuq83th collected 560 from pot
*** SUMMARY ***
Total pot 560 | Rake 0 
Board [Ad 8d 9s 8c 2s]
Seat 2: krychuq83th (big blind) showed [5s 9d] and won (560) with two pair, Nines and Eights
Seat 3: paam66 (button) (small blind) showed [Jd Kd] and lost with a pair of Eights



PokerStars Hand #165668977641: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:02:41 CET [2017/02/04 11:02:41 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (560 in chips) 
Seat 3: paam66 (940 in chips) 
krychuq83th: posts small blind 20
paam66: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [7s 5d]
krychuq83th: folds 
Uncalled bet (20) returned to paam66
paam66 collected 40 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 40 | Rake 0 
Seat 2: krychuq83th (button) (small blind) folded before Flop
Seat 3: paam66 (big blind) collected (40)



PokerStars Hand #165668980903: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:02:45 CET [2017/02/04 11:02:45 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (540 in chips) 
Seat 3: paam66 (960 in chips) 
paam66: posts small blind 20
krychuq83th: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [7h Ac]
paam66: raises 40 to 80
krychuq83th: calls 40
*** FLOP *** [5c 5s Qh]
krychuq83th: checks 
paam66: bets 40
krychuq83th: folds 
Uncalled bet (40) returned to paam66
paam66 collected 160 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 160 | Rake 0 
Board [5c 5s Qh]
Seat 2: krychuq83th (big blind) folded on the Flop
Seat 3: paam66 (button) (small blind) collected (160)



PokerStars Hand #165668988220: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:02:54 CET [2017/02/04 11:02:54 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (460 in chips) 
Seat 3: paam66 (1040 in chips) 
krychuq83th: posts small blind 20
paam66: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [9d 7h]
krychuq83th: folds 
Uncalled bet (20) returned to paam66
paam66 collected 40 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 40 | Rake 0 
Seat 2: krychuq83th (button) (small blind) folded before Flop
Seat 3: paam66 (big blind) collected (40)



PokerStars Hand #165668991199: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:02:58 CET [2017/02/04 11:02:58 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (440 in chips) 
Seat 3: paam66 (1060 in chips) 
paam66: posts small blind 20
krychuq83th: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [2c 5c]
paam66: folds 
Uncalled bet (20) returned to krychuq83th
krychuq83th collected 40 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 40 | Rake 0 
Seat 2: krychuq83th (big blind) collected (40)
Seat 3: paam66 (button) (small blind) folded before Flop



PokerStars Hand #165668994300: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:03:02 CET [2017/02/04 11:03:02 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (460 in chips) 
Seat 3: paam66 (1040 in chips) 
krychuq83th: posts small blind 20
paam66: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [2h 9h]
krychuq83th: folds 
Uncalled bet (20) returned to paam66
paam66 collected 40 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 40 | Rake 0 
Seat 2: krychuq83th (button) (small blind) folded before Flop
Seat 3: paam66 (big blind) collected (40)



PokerStars Hand #165668997082: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:03:05 CET [2017/02/04 11:03:05 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (440 in chips) 
Seat 3: paam66 (1060 in chips) 
paam66: posts small blind 20
krychuq83th: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [6c 7h]
paam66: raises 40 to 80
krychuq83th: folds 
Uncalled bet (40) returned to paam66
paam66 collected 80 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 80 | Rake 0 
Seat 2: krychuq83th (big blind) folded before Flop
Seat 3: paam66 (button) (small blind) collected (80)



PokerStars Hand #165669001984: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:03:11 CET [2017/02/04 11:03:11 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (400 in chips) 
Seat 3: paam66 (1100 in chips) 
krychuq83th: posts small blind 20
paam66: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [Ad Tc]
krychuq83th: raises 360 to 400 and is all-in
paam66: folds 
Uncalled bet (360) returned to krychuq83th
krychuq83th collected 80 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 80 | Rake 0 
Seat 2: krychuq83th (button) (small blind) collected (80)
Seat 3: paam66 (big blind) folded before Flop



PokerStars Hand #165669005713: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:03:16 CET [2017/02/04 11:03:16 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (440 in chips) 
Seat 3: paam66 (1060 in chips) 
paam66: posts small blind 20
krychuq83th: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [7c 6d]
paam66: calls 20
krychuq83th: checks 
*** FLOP *** [6c 8c Tc]
krychuq83th: bets 400 and is all-in
paam66: folds 
Uncalled bet (400) returned to krychuq83th
krychuq83th collected 80 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 80 | Rake 0 
Board [6c 8c Tc]
Seat 2: krychuq83th (big blind) collected (80)
Seat 3: paam66 (button) (small blind) folded on the Flop



PokerStars Hand #165669015698: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:03:28 CET [2017/02/04 11:03:28 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (480 in chips) 
Seat 3: paam66 (1020 in chips) 
krychuq83th: posts small blind 20
paam66: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [6d 6s]
krychuq83th: raises 440 to 480 and is all-in
paam66: folds 
Uncalled bet (440) returned to krychuq83th
krychuq83th collected 80 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 80 | Rake 0 
Seat 2: krychuq83th (button) (small blind) collected (80)
Seat 3: paam66 (big blind) folded before Flop



PokerStars Hand #165669018917: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:03:32 CET [2017/02/04 11:03:32 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (520 in chips) 
Seat 3: paam66 (980 in chips) 
paam66: posts small blind 20
krychuq83th: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [4d 2d]
paam66: folds 
Uncalled bet (20) returned to krychuq83th
krychuq83th collected 40 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 40 | Rake 0 
Seat 2: krychuq83th (big blind) collected (40)
Seat 3: paam66 (button) (small blind) folded before Flop



PokerStars Hand #165669021904: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:03:36 CET [2017/02/04 11:03:36 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (540 in chips) 
Seat 3: paam66 (960 in chips) 
krychuq83th: posts small blind 20
paam66: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [2s Th]
krychuq83th: raises 40 to 80
paam66: calls 40
*** FLOP *** [3s Kc 7s]
paam66: checks 
krychuq83th: checks 
*** TURN *** [3s Kc 7s] [5d]
paam66: checks 
krychuq83th: checks 
*** RIVER *** [3s Kc 7s 5d] [5s]
paam66: bets 40
krychuq83th: folds 
Uncalled bet (40) returned to paam66
paam66 collected 160 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 160 | Rake 0 
Board [3s Kc 7s 5d 5s]
Seat 2: krychuq83th (button) (small blind) folded on the River
Seat 3: paam66 (big blind) collected (160)



PokerStars Hand #165669034982: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:03:52 CET [2017/02/04 11:03:52 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (460 in chips) 
Seat 3: paam66 (1040 in chips) 
paam66: posts small blind 20
krychuq83th: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [Jd 5h]
paam66: raises 40 to 80
krychuq83th: folds 
Uncalled bet (40) returned to paam66
paam66 collected 80 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 80 | Rake 0 
Seat 2: krychuq83th (big blind) folded before Flop
Seat 3: paam66 (button) (small blind) collected (80)



PokerStars Hand #165669042965: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:04:02 CET [2017/02/04 11:04:02 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (420 in chips) 
Seat 3: paam66 (1080 in chips) 
krychuq83th: posts small blind 20
paam66: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [8c 7h]
krychuq83th: folds 
Uncalled bet (20) returned to paam66
paam66 collected 40 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 40 | Rake 0 
Seat 2: krychuq83th (button) (small blind) folded before Flop
Seat 3: paam66 (big blind) collected (40)



PokerStars Hand #165669046756: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:04:06 CET [2017/02/04 11:04:06 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (400 in chips) 
Seat 3: paam66 (1100 in chips) 
paam66: posts small blind 20
krychuq83th: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [8c 4d]
paam66: raises 40 to 80
krychuq83th: folds 
Uncalled bet (40) returned to paam66
paam66 collected 80 from pot
paam66: doesn't show hand 
*** SUMMARY ***
Total pot 80 | Rake 0 
Seat 2: krychuq83th (big blind) folded before Flop
Seat 3: paam66 (button) (small blind) collected (80)



PokerStars Hand #165669052508: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:04:13 CET [2017/02/04 11:04:13 ET]
Table '1814034223 1' 3-max Seat #2 is the button
Seat 2: krychuq83th (360 in chips) 
Seat 3: paam66 (1140 in chips) 
krychuq83th: posts small blind 20
paam66: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [6h 6d]
krychuq83th: raises 320 to 360 and is all-in
paam66: calls 320
*** FLOP *** [2s Kd 8h]
*** TURN *** [2s Kd 8h] [Td]
*** RIVER *** [2s Kd 8h Td] [4c]
*** SHOW DOWN ***
paam66: shows [Qh 9s] (high card King)
krychuq83th: shows [6h 6d] (a pair of Sixes)
krychuq83th collected 720 from pot
*** SUMMARY ***
Total pot 720 | Rake 0 
Board [2s Kd 8h Td 4c]
Seat 2: krychuq83th (button) (small blind) showed [6h 6d] and won (720) with a pair of Sixes
Seat 3: paam66 (big blind) showed [Qh 9s] and lost with high card King



PokerStars Hand #165669065613: Tournament #1814034223, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 17:04:29 CET [2017/02/04 11:04:29 ET]
Table '1814034223 1' 3-max Seat #3 is the button
Seat 2: krychuq83th (720 in chips) 
Seat 3: paam66 (780 in chips) 
paam66: posts small blind 20
krychuq83th: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [2h 2s]
paam66: calls 20
krychuq83th: raises 680 to 720 and is all-in
paam66: calls 680
*** FLOP *** [8h 9d 4s]
*** TURN *** [8h 9d 4s] [Kc]
*** RIVER *** [8h 9d 4s Kc] [Ad]
*** SHOW DOWN ***
krychuq83th: shows [2h 2s] (a pair of Deuces)
paam66: shows [Ac Jc] (a pair of Aces)
paam66 collected 1440 from pot
krychuq83th finished the tournament in 2nd place
paam66 wins the tournament and receives $14.00 - congratulations!
*** SUMMARY ***
Total pot 1440 | Rake 0 
Board [8h 9d 4s Kc Ad]
Seat 2: krychuq83th (big blind) showed [2h 2s] and lost with a pair of Deuces
Seat 3: paam66 (button) (small blind) showed [Ac Jc] and won (1440) with a pair of Aces



