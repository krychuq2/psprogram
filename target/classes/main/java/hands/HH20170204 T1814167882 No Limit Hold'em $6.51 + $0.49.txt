﻿PokerStars Hand #165675272294: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 19:04:15 CET [2017/02/04 13:04:15 ET]
Table '1814167882 1' 3-max Seat #1 is the button
Seat 1: krychuq83th (500 in chips) 
Seat 2: ThUgZNevaDiE (500 in chips) 
Seat 3: sergol244 (500 in chips) 
ThUgZNevaDiE: posts small blind 10
sergol244: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [8c 7h]
krychuq83th: folds 
ThUgZNevaDiE: calls 10
sergol244: raises 40 to 60
ThUgZNevaDiE: raises 440 to 500 and is all-in
sergol244: folds 
Uncalled bet (440) returned to ThUgZNevaDiE
ThUgZNevaDiE collected 120 from pot
ThUgZNevaDiE: doesn't show hand 
*** SUMMARY ***
Total pot 120 | Rake 0 
Seat 1: krychuq83th (button) folded before Flop (didn't bet)
Seat 2: ThUgZNevaDiE (small blind) collected (120)
Seat 3: sergol244 (big blind) folded before Flop



PokerStars Hand #165675296236: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 19:04:41 CET [2017/02/04 13:04:41 ET]
Table '1814167882 1' 3-max Seat #2 is the button
Seat 1: krychuq83th (500 in chips) 
Seat 2: ThUgZNevaDiE (560 in chips) 
Seat 3: sergol244 (440 in chips) 
sergol244: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Jc Qd]
ThUgZNevaDiE: folds 
sergol244: calls 10
krychuq83th: checks 
*** FLOP *** [9c 2c 4h]
sergol244: checks 
krychuq83th: checks 
*** TURN *** [9c 2c 4h] [6h]
sergol244: bets 40
krychuq83th: calls 40
*** RIVER *** [9c 2c 4h 6h] [2d]
sergol244: bets 60
krychuq83th: folds 
Uncalled bet (60) returned to sergol244
sergol244 collected 120 from pot
sergol244: doesn't show hand 
*** SUMMARY ***
Total pot 120 | Rake 0 
Board [9c 2c 4h 6h 2d]
Seat 1: krychuq83th (big blind) folded on the River
Seat 2: ThUgZNevaDiE (button) folded before Flop (didn't bet)
Seat 3: sergol244 (small blind) collected (120)



PokerStars Hand #165675319965: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 19:05:06 CET [2017/02/04 13:05:06 ET]
Table '1814167882 1' 3-max Seat #3 is the button
Seat 1: krychuq83th (440 in chips) 
Seat 2: ThUgZNevaDiE (560 in chips) 
Seat 3: sergol244 (500 in chips) 
krychuq83th: posts small blind 10
ThUgZNevaDiE: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Tc Kh]
sergol244: raises 20 to 40
krychuq83th: folds 
ThUgZNevaDiE: folds 
Uncalled bet (20) returned to sergol244
sergol244 collected 50 from pot
sergol244: doesn't show hand 
*** SUMMARY ***
Total pot 50 | Rake 0 
Seat 1: krychuq83th (small blind) folded before Flop
Seat 2: ThUgZNevaDiE (big blind) folded before Flop
Seat 3: sergol244 (button) collected (50)



PokerStars Hand #165675325899: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 19:05:12 CET [2017/02/04 13:05:12 ET]
Table '1814167882 1' 3-max Seat #1 is the button
Seat 1: krychuq83th (430 in chips) 
Seat 2: ThUgZNevaDiE (540 in chips) 
Seat 3: sergol244 (530 in chips) 
ThUgZNevaDiE: posts small blind 10
sergol244: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [5c 2d]
krychuq83th: folds 
ThUgZNevaDiE: raises 20 to 40
sergol244: calls 20
*** FLOP *** [Ac 9d 6s]
ThUgZNevaDiE: bets 40
sergol244: folds 
Uncalled bet (40) returned to ThUgZNevaDiE
ThUgZNevaDiE collected 80 from pot
ThUgZNevaDiE: doesn't show hand 
*** SUMMARY ***
Total pot 80 | Rake 0 
Board [Ac 9d 6s]
Seat 1: krychuq83th (button) folded before Flop (didn't bet)
Seat 2: ThUgZNevaDiE (small blind) collected (80)
Seat 3: sergol244 (big blind) folded on the Flop



PokerStars Hand #165675351237: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 19:05:39 CET [2017/02/04 13:05:39 ET]
Table '1814167882 1' 3-max Seat #2 is the button
Seat 1: krychuq83th (430 in chips) 
Seat 2: ThUgZNevaDiE (580 in chips) 
Seat 3: sergol244 (490 in chips) 
sergol244: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [8s Qs]
ThUgZNevaDiE: raises 20 to 40
sergol244: folds 
krychuq83th: calls 20
*** FLOP *** [Ks 9h 5s]
krychuq83th: checks 
ThUgZNevaDiE: bets 45
krychuq83th: calls 45
*** TURN *** [Ks 9h 5s] [9d]
krychuq83th: checks 
ThUgZNevaDiE: checks 
*** RIVER *** [Ks 9h 5s 9d] [Kh]
krychuq83th: checks 
ThUgZNevaDiE: checks 
*** SHOW DOWN ***
krychuq83th: shows [8s Qs] (two pair, Kings and Nines)
ThUgZNevaDiE: shows [Ad 7h] (two pair, Kings and Nines - Ace kicker)
ThUgZNevaDiE collected 180 from pot
*** SUMMARY ***
Total pot 180 | Rake 0 
Board [Ks 9h 5s 9d Kh]
Seat 1: krychuq83th (big blind) showed [8s Qs] and lost with two pair, Kings and Nines
Seat 2: ThUgZNevaDiE (button) showed [Ad 7h] and won (180) with two pair, Kings and Nines
Seat 3: sergol244 (small blind) folded before Flop



PokerStars Hand #165675386422: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 19:06:17 CET [2017/02/04 13:06:17 ET]
Table '1814167882 1' 3-max Seat #3 is the button
Seat 1: krychuq83th (345 in chips) 
Seat 2: ThUgZNevaDiE (675 in chips) 
Seat 3: sergol244 (480 in chips) 
krychuq83th: posts small blind 10
ThUgZNevaDiE: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Kh 2h]
sergol244: raises 20 to 40
krychuq83th: folds 
ThUgZNevaDiE: folds 
Uncalled bet (20) returned to sergol244
sergol244 collected 50 from pot
sergol244: doesn't show hand 
*** SUMMARY ***
Total pot 50 | Rake 0 
Seat 1: krychuq83th (small blind) folded before Flop
Seat 2: ThUgZNevaDiE (big blind) folded before Flop
Seat 3: sergol244 (button) collected (50)



PokerStars Hand #165675400804: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 19:06:31 CET [2017/02/04 13:06:31 ET]
Table '1814167882 1' 3-max Seat #1 is the button
Seat 1: krychuq83th (335 in chips) 
Seat 2: ThUgZNevaDiE (655 in chips) 
Seat 3: sergol244 (510 in chips) 
ThUgZNevaDiE: posts small blind 10
sergol244: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [4d 9h]
krychuq83th: folds 
ThUgZNevaDiE: calls 10
sergol244: checks 
*** FLOP *** [7h 2h 8s]
ThUgZNevaDiE: bets 20
sergol244: calls 20
*** TURN *** [7h 2h 8s] [2d]
ThUgZNevaDiE: bets 50
sergol244: calls 50
*** RIVER *** [7h 2h 8s 2d] [6h]
ThUgZNevaDiE: bets 59
sergol244: raises 121 to 180
ThUgZNevaDiE: folds 
Uncalled bet (121) returned to sergol244
sergol244 collected 298 from pot
sergol244: doesn't show hand 
*** SUMMARY ***
Total pot 298 | Rake 0 
Board [7h 2h 8s 2d 6h]
Seat 1: krychuq83th (button) folded before Flop (didn't bet)
Seat 2: ThUgZNevaDiE (small blind) folded on the River
Seat 3: sergol244 (big blind) collected (298)



PokerStars Hand #165675458982: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 19:07:33 CET [2017/02/04 13:07:33 ET]
Table '1814167882 1' 3-max Seat #2 is the button
Seat 1: krychuq83th (335 in chips) 
Seat 2: ThUgZNevaDiE (506 in chips) 
Seat 3: sergol244 (659 in chips) 
sergol244: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [6d 3c]
ThUgZNevaDiE: folds 
sergol244: raises 30 to 60
krychuq83th: folds 
Uncalled bet (30) returned to sergol244
sergol244 collected 60 from pot
sergol244: doesn't show hand 
*** SUMMARY ***
Total pot 60 | Rake 0 
Seat 1: krychuq83th (big blind) folded before Flop
Seat 2: ThUgZNevaDiE (button) folded before Flop (didn't bet)
Seat 3: sergol244 (small blind) collected (60)



PokerStars Hand #165675467123: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 19:07:42 CET [2017/02/04 13:07:42 ET]
Table '1814167882 1' 3-max Seat #3 is the button
Seat 1: krychuq83th (305 in chips) 
Seat 2: ThUgZNevaDiE (506 in chips) 
Seat 3: sergol244 (689 in chips) 
krychuq83th: posts small blind 15
ThUgZNevaDiE: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [6s 7c]
sergol244: raises 30 to 60
krychuq83th: folds 
ThUgZNevaDiE: calls 30
*** FLOP *** [4h Ah Jd]
ThUgZNevaDiE: checks 
sergol244: bets 60
ThUgZNevaDiE: calls 60
*** TURN *** [4h Ah Jd] [7h]
ThUgZNevaDiE: checks 
sergol244: bets 150
ThUgZNevaDiE: raises 236 to 386 and is all-in
sergol244: folds 
Uncalled bet (236) returned to ThUgZNevaDiE
ThUgZNevaDiE collected 555 from pot
ThUgZNevaDiE: doesn't show hand 
*** SUMMARY ***
Total pot 555 | Rake 0 
Board [4h Ah Jd 7h]
Seat 1: krychuq83th (small blind) folded before Flop
Seat 2: ThUgZNevaDiE (big blind) collected (555)
Seat 3: sergol244 (button) folded on the Turn



PokerStars Hand #165675533190: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 19:08:52 CET [2017/02/04 13:08:52 ET]
Table '1814167882 1' 3-max Seat #1 is the button
Seat 1: krychuq83th (290 in chips) 
Seat 2: ThUgZNevaDiE (791 in chips) 
Seat 3: sergol244 (419 in chips) 
ThUgZNevaDiE: posts small blind 15
sergol244: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [2d 2s]
krychuq83th: raises 260 to 290 and is all-in
ThUgZNevaDiE: folds 
sergol244: folds 
Uncalled bet (260) returned to krychuq83th
krychuq83th collected 75 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 75 | Rake 0 
Seat 1: krychuq83th (button) collected (75)
Seat 2: ThUgZNevaDiE (small blind) folded before Flop
Seat 3: sergol244 (big blind) folded before Flop



PokerStars Hand #165675543757: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 19:09:03 CET [2017/02/04 13:09:03 ET]
Table '1814167882 1' 3-max Seat #2 is the button
Seat 1: krychuq83th (335 in chips) 
Seat 2: ThUgZNevaDiE (776 in chips) 
Seat 3: sergol244 (389 in chips) 
sergol244: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Kd 2d]
ThUgZNevaDiE: raises 30 to 60
sergol244: raises 329 to 389 and is all-in
krychuq83th: folds 
ThUgZNevaDiE: folds 
Uncalled bet (329) returned to sergol244
sergol244 collected 150 from pot
sergol244: doesn't show hand 
*** SUMMARY ***
Total pot 150 | Rake 0 
Seat 1: krychuq83th (big blind) folded before Flop
Seat 2: ThUgZNevaDiE (button) folded before Flop
Seat 3: sergol244 (small blind) collected (150)



PokerStars Hand #165675568278: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 19:09:29 CET [2017/02/04 13:09:29 ET]
Table '1814167882 1' 3-max Seat #3 is the button
Seat 1: krychuq83th (305 in chips) 
Seat 2: ThUgZNevaDiE (716 in chips) 
Seat 3: sergol244 (479 in chips) 
krychuq83th: posts small blind 15
ThUgZNevaDiE: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Ts 8d]
sergol244: folds 
krychuq83th: folds 
Uncalled bet (15) returned to ThUgZNevaDiE
ThUgZNevaDiE collected 30 from pot
ThUgZNevaDiE: doesn't show hand 
*** SUMMARY ***
Total pot 30 | Rake 0 
Seat 1: krychuq83th (small blind) folded before Flop
Seat 2: ThUgZNevaDiE (big blind) collected (30)
Seat 3: sergol244 (button) folded before Flop (didn't bet)



PokerStars Hand #165675574380: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 19:09:36 CET [2017/02/04 13:09:36 ET]
Table '1814167882 1' 3-max Seat #1 is the button
Seat 1: krychuq83th (290 in chips) 
Seat 2: ThUgZNevaDiE (731 in chips) 
Seat 3: sergol244 (479 in chips) 
ThUgZNevaDiE: posts small blind 15
sergol244: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [4h Kd]
krychuq83th: folds 
ThUgZNevaDiE: raises 701 to 731 and is all-in
sergol244: folds 
Uncalled bet (701) returned to ThUgZNevaDiE
ThUgZNevaDiE collected 60 from pot
ThUgZNevaDiE: doesn't show hand 
*** SUMMARY ***
Total pot 60 | Rake 0 
Seat 1: krychuq83th (button) folded before Flop (didn't bet)
Seat 2: ThUgZNevaDiE (small blind) collected (60)
Seat 3: sergol244 (big blind) folded before Flop



PokerStars Hand #165675601541: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 19:10:05 CET [2017/02/04 13:10:05 ET]
Table '1814167882 1' 3-max Seat #2 is the button
Seat 1: krychuq83th (290 in chips) 
Seat 2: ThUgZNevaDiE (761 in chips) 
Seat 3: sergol244 (449 in chips) 
sergol244: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Jd As]
ThUgZNevaDiE: folds 
sergol244: raises 419 to 449 and is all-in
krychuq83th: calls 260 and is all-in
Uncalled bet (159) returned to sergol244
*** FLOP *** [4d Qc Qd]
*** TURN *** [4d Qc Qd] [6s]
*** RIVER *** [4d Qc Qd 6s] [Jh]
*** SHOW DOWN ***
sergol244: shows [3c 3h] (two pair, Queens and Threes)
krychuq83th: shows [Jd As] (two pair, Queens and Jacks)
krychuq83th collected 580 from pot
*** SUMMARY ***
Total pot 580 | Rake 0 
Board [4d Qc Qd 6s Jh]
Seat 1: krychuq83th (big blind) showed [Jd As] and won (580) with two pair, Queens and Jacks
Seat 2: ThUgZNevaDiE (button) folded before Flop (didn't bet)
Seat 3: sergol244 (small blind) showed [3c 3h] and lost with two pair, Queens and Threes



PokerStars Hand #165675619445: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 19:10:25 CET [2017/02/04 13:10:25 ET]
Table '1814167882 1' 3-max Seat #3 is the button
Seat 1: krychuq83th (580 in chips) 
Seat 2: ThUgZNevaDiE (761 in chips) 
Seat 3: sergol244 (159 in chips) 
krychuq83th: posts small blind 20
ThUgZNevaDiE: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [Kh 5d]
sergol244: raises 119 to 159 and is all-in
krychuq83th: folds 
ThUgZNevaDiE: folds 
Uncalled bet (119) returned to sergol244
sergol244 collected 100 from pot
sergol244: doesn't show hand 
*** SUMMARY ***
Total pot 100 | Rake 0 
Seat 1: krychuq83th (small blind) folded before Flop
Seat 2: ThUgZNevaDiE (big blind) folded before Flop
Seat 3: sergol244 (button) collected (100)



PokerStars Hand #165675636703: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 19:10:42 CET [2017/02/04 13:10:42 ET]
Table '1814167882 1' 3-max Seat #1 is the button
Seat 1: krychuq83th (560 in chips) 
Seat 2: ThUgZNevaDiE (721 in chips) 
Seat 3: sergol244 (219 in chips) 
ThUgZNevaDiE: posts small blind 20
sergol244: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [Jh 7s]
krychuq83th: folds 
ThUgZNevaDiE: raises 681 to 721 and is all-in
sergol244: folds 
Uncalled bet (681) returned to ThUgZNevaDiE
ThUgZNevaDiE collected 80 from pot
ThUgZNevaDiE: doesn't show hand 
*** SUMMARY ***
Total pot 80 | Rake 0 
Seat 1: krychuq83th (button) folded before Flop (didn't bet)
Seat 2: ThUgZNevaDiE (small blind) collected (80)
Seat 3: sergol244 (big blind) folded before Flop



PokerStars Hand #165675645630: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 19:10:51 CET [2017/02/04 13:10:51 ET]
Table '1814167882 1' 3-max Seat #2 is the button
Seat 1: krychuq83th (560 in chips) 
Seat 2: ThUgZNevaDiE (761 in chips) 
Seat 3: sergol244 (179 in chips) 
sergol244: posts small blind 20
krychuq83th: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [3h Kh]
ThUgZNevaDiE: calls 40
sergol244: raises 139 to 179 and is all-in
krychuq83th: folds 
ThUgZNevaDiE: calls 139
*** FLOP *** [7d 7s Qh]
*** TURN *** [7d 7s Qh] [7c]
*** RIVER *** [7d 7s Qh 7c] [Tc]
*** SHOW DOWN ***
sergol244: shows [3d Qd] (a full house, Sevens full of Queens)
ThUgZNevaDiE: shows [Jh Qc] (a full house, Sevens full of Queens)
sergol244 collected 199 from pot
ThUgZNevaDiE collected 199 from pot
*** SUMMARY ***
Total pot 398 | Rake 0 
Board [7d 7s Qh 7c Tc]
Seat 1: krychuq83th (big blind) folded before Flop
Seat 2: ThUgZNevaDiE (button) showed [Jh Qc] and won (199) with a full house, Sevens full of Queens
Seat 3: sergol244 (small blind) showed [3d Qd] and won (199) with a full house, Sevens full of Queens



PokerStars Hand #165675681824: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 19:11:30 CET [2017/02/04 13:11:30 ET]
Table '1814167882 1' 3-max Seat #3 is the button
Seat 1: krychuq83th (520 in chips) 
Seat 2: ThUgZNevaDiE (781 in chips) 
Seat 3: sergol244 (199 in chips) 
krychuq83th: posts small blind 20
ThUgZNevaDiE: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [Ts 7s]
sergol244: raises 159 to 199 and is all-in
krychuq83th: folds 
ThUgZNevaDiE: calls 159
*** FLOP *** [Ad Jd 3h]
*** TURN *** [Ad Jd 3h] [7d]
*** RIVER *** [Ad Jd 3h 7d] [9h]
*** SHOW DOWN ***
ThUgZNevaDiE: shows [6h 6c] (a pair of Sixes)
sergol244: shows [2h Ac] (a pair of Aces)
sergol244 collected 418 from pot
*** SUMMARY ***
Total pot 418 | Rake 0 
Board [Ad Jd 3h 7d 9h]
Seat 1: krychuq83th (small blind) folded before Flop
Seat 2: ThUgZNevaDiE (big blind) showed [6h 6c] and lost with a pair of Sixes
Seat 3: sergol244 (button) showed [2h Ac] and won (418) with a pair of Aces



PokerStars Hand #165675696540: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 19:11:45 CET [2017/02/04 13:11:45 ET]
Table '1814167882 1' 3-max Seat #1 is the button
Seat 1: krychuq83th (500 in chips) 
Seat 2: ThUgZNevaDiE (582 in chips) 
Seat 3: sergol244 (418 in chips) 
ThUgZNevaDiE: posts small blind 20
sergol244: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [5h 5s]
krychuq83th: raises 460 to 500 and is all-in
ThUgZNevaDiE: folds 
sergol244: folds 
Uncalled bet (460) returned to krychuq83th
krychuq83th collected 100 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 100 | Rake 0 
Seat 1: krychuq83th (button) collected (100)
Seat 2: ThUgZNevaDiE (small blind) folded before Flop
Seat 3: sergol244 (big blind) folded before Flop



PokerStars Hand #165675712078: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 19:12:01 CET [2017/02/04 13:12:01 ET]
Table '1814167882 1' 3-max Seat #2 is the button
Seat 1: krychuq83th (560 in chips) 
Seat 2: ThUgZNevaDiE (562 in chips) 
Seat 3: sergol244 (378 in chips) 
sergol244: posts small blind 20
krychuq83th: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [3h 3s]
ThUgZNevaDiE: raises 522 to 562 and is all-in
sergol244: folds 
krychuq83th: folds [3h 3s]
Uncalled bet (522) returned to ThUgZNevaDiE
ThUgZNevaDiE collected 100 from pot
ThUgZNevaDiE: doesn't show hand 
*** SUMMARY ***
Total pot 100 | Rake 0 
Seat 1: krychuq83th (big blind) folded before Flop
Seat 2: ThUgZNevaDiE (button) collected (100)
Seat 3: sergol244 (small blind) folded before Flop



PokerStars Hand #165675744940: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 19:12:26 CET [2017/02/04 13:12:26 ET]
Table '1814167882 1' 3-max Seat #3 is the button
Seat 1: krychuq83th (520 in chips) 
Seat 2: ThUgZNevaDiE (622 in chips) 
Seat 3: sergol244 (358 in chips) 
krychuq83th: posts small blind 20
ThUgZNevaDiE: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [Kd Kh]
sergol244: raises 40 to 80
krychuq83th: raises 120 to 200
ThUgZNevaDiE: folds 
sergol244: folds 
Uncalled bet (120) returned to krychuq83th
krychuq83th collected 200 from pot
*** SUMMARY ***
Total pot 200 | Rake 0 
Seat 1: krychuq83th (small blind) collected (200)
Seat 2: ThUgZNevaDiE (big blind) folded before Flop
Seat 3: sergol244 (button) folded before Flop



PokerStars Hand #165675761671: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 19:12:43 CET [2017/02/04 13:12:43 ET]
Table '1814167882 1' 3-max Seat #1 is the button
Seat 1: krychuq83th (640 in chips) 
Seat 2: ThUgZNevaDiE (582 in chips) 
Seat 3: sergol244 (278 in chips) 
ThUgZNevaDiE: posts small blind 20
sergol244: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [7d 6c]
krychuq83th: folds 
ThUgZNevaDiE: folds 
Uncalled bet (20) returned to sergol244
sergol244 collected 40 from pot
sergol244: doesn't show hand 
*** SUMMARY ***
Total pot 40 | Rake 0 
Seat 1: krychuq83th (button) folded before Flop (didn't bet)
Seat 2: ThUgZNevaDiE (small blind) folded before Flop
Seat 3: sergol244 (big blind) collected (40)



PokerStars Hand #165675782689: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 19:13:10 CET [2017/02/04 13:13:10 ET]
Table '1814167882 1' 3-max Seat #2 is the button
Seat 1: krychuq83th (640 in chips) 
Seat 2: ThUgZNevaDiE (562 in chips) 
Seat 3: sergol244 (298 in chips) 
sergol244: posts small blind 20
krychuq83th: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [Qs 6s]
ThUgZNevaDiE: folds 
sergol244: folds 
Uncalled bet (20) returned to krychuq83th
krychuq83th collected 40 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 40 | Rake 0 
Seat 1: krychuq83th (big blind) collected (40)
Seat 2: ThUgZNevaDiE (button) folded before Flop (didn't bet)
Seat 3: sergol244 (small blind) folded before Flop



PokerStars Hand #165675795422: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level IV (30/60) - 2017/02/04 19:13:24 CET [2017/02/04 13:13:24 ET]
Table '1814167882 1' 3-max Seat #3 is the button
Seat 1: krychuq83th (660 in chips) 
Seat 2: ThUgZNevaDiE (562 in chips) 
Seat 3: sergol244 (278 in chips) 
krychuq83th: posts small blind 30
ThUgZNevaDiE: posts big blind 60
*** HOLE CARDS ***
Dealt to krychuq83th [4h 5h]
sergol244: raises 218 to 278 and is all-in
krychuq83th: folds 
ThUgZNevaDiE: folds 
Uncalled bet (218) returned to sergol244
sergol244 collected 150 from pot
sergol244: doesn't show hand 
*** SUMMARY ***
Total pot 150 | Rake 0 
Seat 1: krychuq83th (small blind) folded before Flop
Seat 2: ThUgZNevaDiE (big blind) folded before Flop
Seat 3: sergol244 (button) collected (150)



PokerStars Hand #165675806815: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level IV (30/60) - 2017/02/04 19:13:36 CET [2017/02/04 13:13:36 ET]
Table '1814167882 1' 3-max Seat #1 is the button
Seat 1: krychuq83th (630 in chips) 
Seat 2: ThUgZNevaDiE (502 in chips) 
Seat 3: sergol244 (368 in chips) 
ThUgZNevaDiE: posts small blind 30
sergol244: posts big blind 60
*** HOLE CARDS ***
Dealt to krychuq83th [7c 8h]
krychuq83th: folds 
ThUgZNevaDiE: folds 
Uncalled bet (30) returned to sergol244
sergol244 collected 60 from pot
sergol244: doesn't show hand 
*** SUMMARY ***
Total pot 60 | Rake 0 
Seat 1: krychuq83th (button) folded before Flop (didn't bet)
Seat 2: ThUgZNevaDiE (small blind) folded before Flop
Seat 3: sergol244 (big blind) collected (60)



PokerStars Hand #165675819621: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level IV (30/60) - 2017/02/04 19:13:50 CET [2017/02/04 13:13:50 ET]
Table '1814167882 1' 3-max Seat #2 is the button
Seat 1: krychuq83th (630 in chips) 
Seat 2: ThUgZNevaDiE (472 in chips) 
Seat 3: sergol244 (398 in chips) 
sergol244: posts small blind 30
krychuq83th: posts big blind 60
*** HOLE CARDS ***
Dealt to krychuq83th [3d 3h]
ThUgZNevaDiE: raises 60 to 120
sergol244: raises 278 to 398 and is all-in
krychuq83th: calls 338
ThUgZNevaDiE: raises 74 to 472 and is all-in
krychuq83th: calls 74
*** FLOP *** [4d As 2d]
*** TURN *** [4d As 2d] [2h]
*** RIVER *** [4d As 2d 2h] [6c]
*** SHOW DOWN ***
krychuq83th: shows [3d 3h] (two pair, Threes and Deuces)
ThUgZNevaDiE: shows [Qc Qs] (two pair, Queens and Deuces)
ThUgZNevaDiE collected 148 from side pot
sergol244: shows [Ad Tc] (two pair, Aces and Deuces)
sergol244 collected 1194 from main pot
*** SUMMARY ***
Total pot 1342 Main pot 1194. Side pot 148. | Rake 0 
Board [4d As 2d 2h 6c]
Seat 1: krychuq83th (big blind) showed [3d 3h] and lost with two pair, Threes and Deuces
Seat 2: ThUgZNevaDiE (button) showed [Qc Qs] and won (148) with two pair, Queens and Deuces
Seat 3: sergol244 (small blind) showed [Ad Tc] and won (1194) with two pair, Aces and Deuces



PokerStars Hand #165675855531: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level IV (30/60) - 2017/02/04 19:14:29 CET [2017/02/04 13:14:29 ET]
Table '1814167882 1' 3-max Seat #3 is the button
Seat 1: krychuq83th (158 in chips) 
Seat 2: ThUgZNevaDiE (148 in chips) 
Seat 3: sergol244 (1194 in chips) 
krychuq83th: posts small blind 30
ThUgZNevaDiE: posts big blind 60
*** HOLE CARDS ***
Dealt to krychuq83th [Ac 7h]
sergol244: folds 
krychuq83th: raises 98 to 158 and is all-in
ThUgZNevaDiE: calls 88 and is all-in
Uncalled bet (10) returned to krychuq83th
*** FLOP *** [6d 8s 3d]
*** TURN *** [6d 8s 3d] [Qs]
*** RIVER *** [6d 8s 3d Qs] [9s]
*** SHOW DOWN ***
krychuq83th: shows [Ac 7h] (high card Ace)
ThUgZNevaDiE: shows [Ts Td] (a pair of Tens)
ThUgZNevaDiE collected 296 from pot
*** SUMMARY ***
Total pot 296 | Rake 0 
Board [6d 8s 3d Qs 9s]
Seat 1: krychuq83th (small blind) showed [Ac 7h] and lost with high card Ace
Seat 2: ThUgZNevaDiE (big blind) showed [Ts Td] and won (296) with a pair of Tens
Seat 3: sergol244 (button) folded before Flop (didn't bet)



PokerStars Hand #165675871720: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level IV (30/60) - 2017/02/04 19:14:46 CET [2017/02/04 13:14:46 ET]
Table '1814167882 1' 3-max Seat #1 is the button
Seat 1: krychuq83th (10 in chips) 
Seat 2: ThUgZNevaDiE (296 in chips) 
Seat 3: sergol244 (1194 in chips) 
ThUgZNevaDiE: posts small blind 30
sergol244: posts big blind 60
*** HOLE CARDS ***
Dealt to krychuq83th [Qh As]
krychuq83th: calls 10 and is all-in
ThUgZNevaDiE: raises 236 to 296 and is all-in
sergol244: folds 
Uncalled bet (236) returned to ThUgZNevaDiE
*** FLOP *** [9h Qc Kd]
*** TURN *** [9h Qc Kd] [Qd]
*** RIVER *** [9h Qc Kd Qd] [7s]
*** SHOW DOWN ***
ThUgZNevaDiE: shows [Kh Ah] (two pair, Kings and Queens)
ThUgZNevaDiE collected 100 from side pot
krychuq83th: shows [Qh As] (three of a kind, Queens)
krychuq83th collected 30 from main pot
*** SUMMARY ***
Total pot 130 Main pot 30. Side pot 100. | Rake 0 
Board [9h Qc Kd Qd 7s]
Seat 1: krychuq83th (button) showed [Qh As] and won (30) with three of a kind, Queens
Seat 2: ThUgZNevaDiE (small blind) showed [Kh Ah] and won (100) with two pair, Kings and Queens
Seat 3: sergol244 (big blind) folded before Flop



PokerStars Hand #165675886797: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level IV (30/60) - 2017/02/04 19:15:02 CET [2017/02/04 13:15:02 ET]
Table '1814167882 1' 3-max Seat #2 is the button
Seat 1: krychuq83th (30 in chips) 
Seat 2: ThUgZNevaDiE (336 in chips) 
Seat 3: sergol244 (1134 in chips) 
sergol244: posts small blind 30
krychuq83th: posts big blind 30 and is all-in
*** HOLE CARDS ***
Dealt to krychuq83th [Qc 3h]
ThUgZNevaDiE: raises 306 to 336 and is all-in
sergol244: folds 
Uncalled bet (306) returned to ThUgZNevaDiE
*** FLOP *** [5s 3c Jc]
*** TURN *** [5s 3c Jc] [2h]
*** RIVER *** [5s 3c Jc 2h] [Jd]
*** SHOW DOWN ***
krychuq83th: shows [Qc 3h] (two pair, Jacks and Threes)
ThUgZNevaDiE: shows [Ks Ad] (a pair of Jacks)
krychuq83th collected 90 from pot
*** SUMMARY ***
Total pot 90 | Rake 0 
Board [5s 3c Jc 2h Jd]
Seat 1: krychuq83th (big blind) showed [Qc 3h] and won (90) with two pair, Jacks and Threes
Seat 2: ThUgZNevaDiE (button) showed [Ks Ad] and lost with a pair of Jacks
Seat 3: sergol244 (small blind) folded before Flop



PokerStars Hand #165675899953: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level IV (30/60) - 2017/02/04 19:15:15 CET [2017/02/04 13:15:15 ET]
Table '1814167882 1' 3-max Seat #3 is the button
Seat 1: krychuq83th (90 in chips) 
Seat 2: ThUgZNevaDiE (306 in chips) 
Seat 3: sergol244 (1104 in chips) 
krychuq83th: posts small blind 30
ThUgZNevaDiE: posts big blind 60
*** HOLE CARDS ***
Dealt to krychuq83th [2d 7d]
sergol244: raises 1044 to 1104 and is all-in
krychuq83th: calls 60 and is all-in
ThUgZNevaDiE: folds 
Uncalled bet (1014) returned to sergol244
*** FLOP *** [5c 8h 8c]
*** TURN *** [5c 8h 8c] [5h]
*** RIVER *** [5c 8h 8c 5h] [Kc]
*** SHOW DOWN ***
krychuq83th: shows [2d 7d] (two pair, Eights and Fives)
sergol244: shows [3s 3h] (two pair, Eights and Fives)
krychuq83th collected 120 from pot
sergol244 collected 120 from pot
*** SUMMARY ***
Total pot 240 | Rake 0 
Board [5c 8h 8c 5h Kc]
Seat 1: krychuq83th (small blind) showed [2d 7d] and won (120) with two pair, Eights and Fives
Seat 2: ThUgZNevaDiE (big blind) folded before Flop
Seat 3: sergol244 (button) showed [3s 3h] and won (120) with two pair, Eights and Fives



PokerStars Hand #165675922852: Tournament #1814167882, $6.51+$0.49 USD Hold'em No Limit - Level IV (30/60) - 2017/02/04 19:15:39 CET [2017/02/04 13:15:39 ET]
Table '1814167882 1' 3-max Seat #1 is the button
Seat 1: krychuq83th (120 in chips) 
Seat 2: ThUgZNevaDiE (246 in chips) 
Seat 3: sergol244 (1134 in chips) 
ThUgZNevaDiE: posts small blind 30
sergol244: posts big blind 60
*** HOLE CARDS ***
Dealt to krychuq83th [Qd Kh]
krychuq83th: raises 60 to 120 and is all-in
ThUgZNevaDiE: folds 
sergol244: calls 60
*** FLOP *** [4d 8c Js]
*** TURN *** [4d 8c Js] [Ad]
*** RIVER *** [4d 8c Js Ad] [6h]
*** SHOW DOWN ***
sergol244: shows [8s As] (two pair, Aces and Eights)
krychuq83th: shows [Qd Kh] (high card Ace)
sergol244 collected 270 from pot
*** SUMMARY ***
Total pot 270 | Rake 0 
Board [4d 8c Js Ad 6h]
Seat 1: krychuq83th (button) showed [Qd Kh] and lost with high card Ace
Seat 2: ThUgZNevaDiE (small blind) folded before Flop
Seat 3: sergol244 (big blind) showed [8s As] and won (270) with two pair, Aces and Eights



