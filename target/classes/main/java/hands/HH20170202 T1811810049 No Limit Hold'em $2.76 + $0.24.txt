﻿﻿PokerStars Hand #165565837858: Tournament #1811810049, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/02 18:51:45 CET [2017/02/02 12:51:45 ET]
Table '1811810049 1' 3-max Seat #1 is the button
Seat 1: Igors_1903 (500 in chips) 
Seat 2: krychuq83th (500 in chips) 
Seat 3: jakysacaan (500 in chips) 
krychuq83th: posts small blind 10
jakysacaan: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Qh Qd]
Igors_1903: raises 20 to 40
krychuq83th: raises 460 to 500 and is all-in
jakysacaan: folds 
Igors_1903: folds 
Uncalled bet (460) returned to krychuq83th
krychuq83th collected 100 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 100 | Rake 0 
Seat 1: Igors_1903 (button) folded before Flop
Seat 2: krychuq83th (small blind) collected (100)
Seat 3: jakysacaan (big blind) folded before Flop



PokerStars Hand #165565852221: Tournament #1811810049, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/02 18:52:01 CET [2017/02/02 12:52:01 ET]
Table '1811810049 1' 3-max Seat #2 is the button
Seat 1: Igors_1903 (460 in chips) 
Seat 2: krychuq83th (560 in chips) 
Seat 3: jakysacaan (480 in chips) 
jakysacaan: posts small blind 10
Igors_1903: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Td 4s]
krychuq83th: folds 
jakysacaan: folds 
Uncalled bet (10) returned to Igors_1903
Igors_1903 collected 20 from pot
Igors_1903: doesn't show hand 
*** SUMMARY ***
Total pot 20 | Rake 0 
Seat 1: Igors_1903 (big blind) collected (20)
Seat 2: krychuq83th (button) folded before Flop (didn't bet)
Seat 3: jakysacaan (small blind) folded before Flop



PokerStars Hand #165565857242: Tournament #1811810049, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/02 18:52:07 CET [2017/02/02 12:52:07 ET]
Table '1811810049 1' 3-max Seat #3 is the button
Seat 1: Igors_1903 (470 in chips) 
Seat 2: krychuq83th (560 in chips) 
Seat 3: jakysacaan (470 in chips) 
Igors_1903: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [9h 5d]
jakysacaan: raises 20 to 40
Igors_1903: folds 
krychuq83th: folds 
Uncalled bet (20) returned to jakysacaan
jakysacaan collected 50 from pot
*** SUMMARY ***
Total pot 50 | Rake 0 
Seat 1: Igors_1903 (small blind) folded before Flop
Seat 2: krychuq83th (big blind) folded before Flop
Seat 3: jakysacaan (button) collected (50)



PokerStars Hand #165565870775: Tournament #1811810049, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/02 18:52:22 CET [2017/02/02 12:52:22 ET]
Table '1811810049 1' 3-max Seat #1 is the button
Seat 1: Igors_1903 (460 in chips) 
Seat 2: krychuq83th (540 in chips) 
Seat 3: jakysacaan (500 in chips) 
krychuq83th: posts small blind 10
jakysacaan: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Kc 9h]
Igors_1903: folds 
krychuq83th: calls 10
jakysacaan: checks 
*** FLOP *** [3d Ac 7s]
krychuq83th: checks 
jakysacaan: bets 40
krychuq83th: folds 
Uncalled bet (40) returned to jakysacaan
jakysacaan collected 40 from pot
*** SUMMARY ***
Total pot 40 | Rake 0 
Board [3d Ac 7s]
Seat 1: Igors_1903 (button) folded before Flop (didn't bet)
Seat 2: krychuq83th (small blind) folded on the Flop
Seat 3: jakysacaan (big blind) collected (40)



PokerStars Hand #165565887055: Tournament #1811810049, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/02 18:52:40 CET [2017/02/02 12:52:40 ET]
Table '1811810049 1' 3-max Seat #2 is the button
Seat 1: Igors_1903 (460 in chips) 
Seat 2: krychuq83th (520 in chips) 
Seat 3: jakysacaan (520 in chips) 
jakysacaan: posts small blind 10
Igors_1903: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [6h Tc]
krychuq83th: folds 
jakysacaan: calls 10
Igors_1903: raises 40 to 60
jakysacaan: calls 40
*** FLOP *** [Kc 5h 3c]
jakysacaan: checks 
Igors_1903: bets 53
jakysacaan: folds 
Uncalled bet (53) returned to Igors_1903
Igors_1903 collected 120 from pot
Igors_1903: doesn't show hand 
*** SUMMARY ***
Total pot 120 | Rake 0 
Board [Kc 5h 3c]
Seat 1: Igors_1903 (big blind) collected (120)
Seat 2: krychuq83th (button) folded before Flop (didn't bet)
Seat 3: jakysacaan (small blind) folded on the Flop



PokerStars Hand #165565918450: Tournament #1811810049, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/02 18:53:14 CET [2017/02/02 12:53:14 ET]
Table '1811810049 1' 3-max Seat #3 is the button
Seat 1: Igors_1903 (520 in chips) 
Seat 2: krychuq83th (520 in chips) 
Seat 3: jakysacaan (460 in chips) 
Igors_1903: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [4h 4d]
jakysacaan: raises 20 to 40
Igors_1903: folds 
krychuq83th: raises 480 to 520 and is all-in
jakysacaan: calls 420 and is all-in
Uncalled bet (60) returned to krychuq83th
*** FLOP *** [Kh 2c Tc]
*** TURN *** [Kh 2c Tc] [Qs]
*** RIVER *** [Kh 2c Tc Qs] [Jd]
*** SHOW DOWN ***
krychuq83th: shows [4h 4d] (a pair of Fours)
jakysacaan: shows [As Ts] (a straight, Ten to Ace)
jakysacaan collected 930 from pot
*** SUMMARY ***
Total pot 930 | Rake 0 
Board [Kh 2c Tc Qs Jd]
Seat 1: Igors_1903 (small blind) folded before Flop
Seat 2: krychuq83th (big blind) showed [4h 4d] and lost with a pair of Fours
Seat 3: jakysacaan (button) showed [As Ts] and won (930) with a straight, Ten to Ace



PokerStars Hand #165565934869: Tournament #1811810049, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/02 18:53:32 CET [2017/02/02 12:53:32 ET]
Table '1811810049 1' 3-max Seat #1 is the button
Seat 1: Igors_1903 (510 in chips) 
Seat 2: krychuq83th (60 in chips) 
Seat 3: jakysacaan (930 in chips) 
krychuq83th: posts small blind 10
jakysacaan: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [4h 5h]
Igors_1903: folds 
krychuq83th: raises 40 to 60 and is all-in
jakysacaan: calls 40
*** FLOP *** [2s 9c Jc]
*** TURN *** [2s 9c Jc] [4d]
*** RIVER *** [2s 9c Jc 4d] [Tc]
*** SHOW DOWN ***
krychuq83th: shows [4h 5h] (a pair of Fours)
jakysacaan: shows [Jd Qh] (a pair of Jacks)
jakysacaan collected 120 from pot
krychuq83th finished the tournament in 3rd place
*** SUMMARY ***
Total pot 120 | Rake 0 
Board [2s 9c Jc 4d Tc]
Seat 1: Igors_1903 (button) folded before Flop (didn't bet)
Seat 2: krychuq83th (small blind) showed [4h 5h] and lost with a pair of Fours
Seat 3: jakysacaan (big blind) showed [Jd Qh] and won (120) with a pair of Jacks



