﻿﻿PokerStars Hand #165571850944: Tournament #1811934818, $1.34+$1.34+$0.32 USD Hold'em No Limit - Level I (25/50) - 2017/02/02 20:36:46 CET [2017/02/02 14:36:46 ET]
Table '1811934818 3' 6-max Seat #1 is the button
Seat 1: SNAIPER12 (500 in chips) 
Seat 2: crazyroli (500 in chips) 
Seat 3: russteel (500 in chips) 
Seat 4: krychuq83th (500 in chips) 
Seat 5: grifon3007 (500 in chips) 
Seat 6: vova1222 (500 in chips) 
SNAIPER12: posts the ante 10
crazyroli: posts the ante 10
russteel: posts the ante 10
krychuq83th: posts the ante 10
grifon3007: posts the ante 10
vova1222: posts the ante 10
crazyroli: posts small blind 25
russteel: posts big blind 50
*** HOLE CARDS ***
Dealt to krychuq83th [4c 4s]
krychuq83th: raises 440 to 490 and is all-in
grifon3007: calls 490 and is all-in
vova1222: folds 
SNAIPER12: folds 
crazyroli: folds 
russteel: calls 440 and is all-in
*** FLOP *** [Jd Qs 6c]
*** TURN *** [Jd Qs 6c] [Jc]
*** RIVER *** [Jd Qs 6c Jc] [4d]
*** SHOW DOWN ***
russteel: shows [Qh Js] (a full house, Jacks full of Queens)
krychuq83th: shows [4c 4s] (a full house, Fours full of Jacks)
grifon3007: shows [Qc Kh] (two pair, Queens and Jacks)
russteel collected 1555 from pot
russteel wins the $1.34 bounty for eliminating krychuq83th
russteel wins the $1.34 bounty for eliminating grifon3007
krychuq83th finished the tournament in 22nd place
grifon3007 finished the tournament in 22nd place
*** SUMMARY ***
Total pot 1555 | Rake 0 
Board [Jd Qs 6c Jc 4d]
Seat 1: SNAIPER12 (button) folded before Flop (didn't bet)
Seat 2: crazyroli (small blind) folded before Flop
Seat 3: russteel (big blind) showed [Qh Js] and won (1555) with a full house, Jacks full of Queens
Seat 4: krychuq83th showed [4c 4s] and lost with a full house, Fours full of Jacks
Seat 5: grifon3007 showed [Qc Kh] and lost with two pair, Queens and Jacks
Seat 6: vova1222 folded before Flop (didn't bet)



