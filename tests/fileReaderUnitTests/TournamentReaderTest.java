package fileReaderUnitTests;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import sample.fileReaders.TournamentReader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by krysn on 10.06.2017.
 */
public class TournamentReaderTest {
    TournamentReader tournamentReader;
    File hand;
    Path path;
    String fileName = "HH20170609 T1937741809 No Limit Hold'em $6.96 + $0.14.txt";
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();


    @Before
    public void runBeforeTestMethod() {

        path = Paths.get("c:/users/krysn/desktop/hands/krychuq83th");
        System.out.println("@Before - runBeforeTestMethod");
        tournamentReader = new TournamentReader();
        try {
            hand = folder.newFile("HH20170520 T1918990516 No Limit Hold'emhdjhjjdfh $0.88 + $0.12.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //check if dir exists
    @Test
    public void checkIfDirExists(){
       assertTrue(Files.exists(path));

    }


    @Test
    public void checkTournamentId(){
        //expected id
        int id = 1937741809;
        //id from tested class
        int testedId = Integer.parseInt(tournamentReader.getTournamentId(fileName));
        //result
        boolean test;
        if(testedId == id){
             test = true;
        }else{
            test = false;
        }

        assertTrue(test);

    }
    @Test
    public void checkIfDataCorrect(){
        //expected date
        String expectedDate = "20170609";
        System.out.println(tournamentReader.getDate(fileName));
        assertTrue(expectedDate.equals(tournamentReader.getDate(fileName)));
    }
    @Test
    public void checkTest(){
        //expected double
        Double tournamentCost = 7.1;
        Double cost = tournamentReader.getCost(fileName);
        assertEquals(tournamentCost, cost);
    }

}
