package sample.fileReaders.accountOperations;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;
import sample.service.UserService;

import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.service.query.impl.NodeQueryUtils.hasText;

/**
 * Created by krysn on 14.06.2017.
 */
public class WithdrawTest extends ApplicationTest {
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../views/account/withdraw.fxml"));
        Scene scene = new Scene(root, 539, 416);
        stage.setScene(scene);
        stage.show();
    }
    @Test
    public void test1_checkIfAmountNotEmpty(){
        clickOn("#amount").write("");
        clickOn("#withdrawButton");
        verifyThat("#withdrawError", hasText("Withdraw amount can not be empty"));

    }
    @Test
    public void test2_checkWithInteger(){
        clickOn("#amount").write("1");
        clickOn("#withdrawButton");
        verifyThat("#withdrawError", hasText(""));

    }

    @Test
    public void test3_double(){
        clickOn("#amount").write("1.02");
        clickOn("#withdrawButton");
        verifyThat("#withdrawError", hasText(""));

    }
    @Test
    public void test4_checkZero(){
        clickOn("#amount").write("0");
        clickOn("#withdrawButton");
        verifyThat("#withdrawError", hasText("Withdraw amount needs to be bigger than 0"));
    }
    @Test
    public void test5_checkLetters(){
        clickOn("#amount").write("dddd");
        clickOn("#withdrawButton");
        verifyThat("#withdrawError", hasText("Withdraw amount needs to be a number"));
    }
    @Test
    public void test6_checkMinus(){
        clickOn("#amount").write("-2");
        clickOn("#withdrawButton");
        verifyThat("#withdrawError", hasText("Withdraw amount needs to be bigger than 0"));
    }
}
