package sample.fileReaders;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.testfx.framework.junit.ApplicationTest;

import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.service.query.impl.NodeQueryUtils.hasText;

/**
 * Created by krysn on 04.06.2017.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegisterViewTest extends ApplicationTest {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("views/account/registerView.fxml"));
        Scene scene = new Scene(root, 620, 630);
        stage.setScene(scene);
        stage.show();
    }

    @Test
    public void test2_ClickRegister(){
        clickOn("#register");
        verifyThat("#loginError", hasText("username is required"));
        verifyThat("#balanceError", hasText("balance is required"));
        verifyThat("#passwordError", hasText("password is required"));
        verifyThat("#pathError", hasText("tournaments location is required"));


    }
    @Test
    public void test3_InsertToShortLogin(){
        clickOn("#login").write("hh");
        clickOn("#register");
        verifyThat("#loginError", hasText("username should be at least 3 long"));

    }
    @Test public void test4_InsertCorrectLogin(){
        clickOn("#login").write("hhh");
        clickOn("#register");
        verifyThat("#loginError", hasText(""));
        verifyThat("#balanceError", hasText("balance is required"));
    }
    @Test public void test5_BalanceTestWrong(){
        clickOn("#balance").write("kupa");
        clickOn("#register");
        verifyThat("#balanceError", hasText("It has to be number"));
    }

    @Test public void test5_BalanceTestCorrect(){
        clickOn("#balance").write("102");
        clickOn("#register");
        verifyThat("#balanceError", hasText(""));
    }


    @Test public void test6_CheckPassword(){
        clickOn("#password").write("hh");
        clickOn("#register");
        verifyThat("#passwordError", hasText("password should be at least 3 long"));
        //sleep(1000);
    }
    @Test public void test7_IdenticalPassword(){
        clickOn("#password").write("hhh");
        clickOn("#repeatPassword").write("hhhh");
        clickOn("#register");
        verifyThat("#passwordError", hasText("password and repeat password field should be identical"));

    }
    @Test public void test8_CorrectPassword(){
        clickOn("#password").write("hhhh");
        clickOn("#repeatPassword").write("hhhh");
        clickOn("#register");
        verifyThat("#passwordError", hasText(""));

    }
    //@Test public void test9_


}
