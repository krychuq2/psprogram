package sample.fileReaders;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.testfx.framework.junit.ApplicationTest;

import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.service.query.impl.NodeQueryUtils.hasText;

/**
 * Created by krysn on 04.06.2017.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginViewTest extends ApplicationTest {
    @Override
    public void start(Stage stage) throws Exception {
        //Parent root = new StackPane();
        Parent root = FXMLLoader.load(getClass().getResource("views/loginView.fxml"));
        Scene scene = new Scene(root, 540, 550);
        stage.setScene(scene);
        stage.show();
    }

    @Test
    public void test3_validCredentials(){
        clickOn("#login").write("hhhh");
        clickOn("#password").write("hhhh");
        clickOn("#submit");

    }


//
    @Test public void test1_should_click_on_button() {
        // when:
        clickOn("#submit");
        verifyThat("#feedback", hasText("wrong user name or password"));

    }
    @Test
    public void test2_checkWrongPassword(){
        clickOn("#login").write("hello");
        clickOn("#password").write("hello");
        clickOn("#submit");
        verifyThat("#feedback", hasText("wrong user name or password"));

    }

//    @Test
//    public void should_contain_button() {
//        // expect:
//        verifyThat("#submit", hasText("GO"));
//
//    }
}