﻿﻿PokerStars Hand #165570443005: Tournament #1811877812, $0.91+$0.09 USD Hold'em No Limit - Level I (10/20) - 2017/02/02 20:13:22 CET [2017/02/02 14:13:22 ET]
Table '1811877812 6' 9-max Seat #1 is the button
Seat 1: Fat Slack (500 in chips) 
Seat 2: dyo_live (500 in chips) 
Seat 3: jstoyanov (500 in chips) 
Seat 4: ocsigsxr (500 in chips) 
Seat 5: gamerq (500 in chips) 
Seat 6: Madsii (500 in chips) 
Seat 7: krychuq83th (500 in chips) 
Seat 8: 2danG4u (500 in chips) 
Seat 9: ara_a83 (500 in chips) 
Fat Slack: posts the ante 2
dyo_live: posts the ante 2
jstoyanov: posts the ante 2
ocsigsxr: posts the ante 2
gamerq: posts the ante 2
Madsii: posts the ante 2
krychuq83th: posts the ante 2
2danG4u: posts the ante 2
ara_a83: posts the ante 2
dyo_live: posts small blind 10
jstoyanov: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Ah 8s]
ocsigsxr: calls 20
gamerq: folds 
Madsii: folds 
krychuq83th has timed out
krychuq83th: folds 
krychuq83th is sitting out
2danG4u: folds 
ara_a83: folds 
Fat Slack: folds 
krychuq83th has returned
dyo_live: calls 10
jstoyanov: checks 
*** FLOP *** [3d 8h Kd]
dyo_live: checks 
jstoyanov has timed out
jstoyanov: checks 
ocsigsxr: bets 78
dyo_live: calls 78
jstoyanov: folds 
*** TURN *** [3d 8h Kd] [6d]
dyo_live: checks 
ocsigsxr: checks 
*** RIVER *** [3d 8h Kd 6d] [5c]
dyo_live: bets 400 and is all-in
ocsigsxr: folds 
Uncalled bet (400) returned to dyo_live
dyo_live collected 234 from pot
dyo_live: doesn't show hand 
*** SUMMARY ***
Total pot 234 | Rake 0 
Board [3d 8h Kd 6d 5c]
Seat 1: Fat Slack (button) folded before Flop (didn't bet)
Seat 2: dyo_live (small blind) collected (234)
Seat 3: jstoyanov (big blind) folded on the Flop
Seat 4: ocsigsxr folded on the River
Seat 5: gamerq folded before Flop (didn't bet)
Seat 6: Madsii folded before Flop (didn't bet)
Seat 7: krychuq83th folded before Flop (didn't bet)
Seat 8: 2danG4u folded before Flop (didn't bet)
Seat 9: ara_a83 folded before Flop (didn't bet)



PokerStars Hand #165570611767: Tournament #1811877812, $0.91+$0.09 USD Hold'em No Limit - Level II (15/30) - 2017/02/02 20:16:13 CET [2017/02/02 14:16:13 ET]
Table '1811877812 6' 9-max Seat #2 is the button
Seat 1: Fat Slack (498 in chips) 
Seat 2: dyo_live (634 in chips) 
Seat 3: jstoyanov (478 in chips) 
Seat 4: ocsigsxr (400 in chips) 
Seat 5: gamerq (498 in chips) 
Seat 6: Madsii (498 in chips) 
Seat 7: krychuq83th (498 in chips) 
Seat 8: 2danG4u (498 in chips) 
Seat 9: ara_a83 (498 in chips) 
Fat Slack: posts the ante 3
dyo_live: posts the ante 3
jstoyanov: posts the ante 3
ocsigsxr: posts the ante 3
gamerq: posts the ante 3
Madsii: posts the ante 3
krychuq83th: posts the ante 3
2danG4u: posts the ante 3
ara_a83: posts the ante 3
jstoyanov: posts small blind 15
ocsigsxr: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Ac Kh]
gamerq: folds 
Madsii: folds 
krychuq83th: raises 60 to 90
2danG4u: folds 
ara_a83: folds 
Fat Slack: folds 
dyo_live: folds 
jstoyanov: folds 
ocsigsxr: calls 60
*** FLOP *** [2c 3d Td]
ocsigsxr: checks 
krychuq83th: bets 111
ocsigsxr: calls 111
*** TURN *** [2c 3d Td] [6c]
ocsigsxr: checks 
krychuq83th: checks 
*** RIVER *** [2c 3d Td 6c] [Ah]
ocsigsxr: bets 196 and is all-in
krychuq83th: calls 196
*** SHOW DOWN ***
ocsigsxr: shows [2h 3s] (two pair, Threes and Deuces)
krychuq83th: shows [Ac Kh] (a pair of Aces)
ocsigsxr collected 836 from pot
*** SUMMARY ***
Total pot 836 | Rake 0 
Board [2c 3d Td 6c Ah]
Seat 1: Fat Slack folded before Flop (didn't bet)
Seat 2: dyo_live (button) folded before Flop (didn't bet)
Seat 3: jstoyanov (small blind) folded before Flop
Seat 4: ocsigsxr (big blind) showed [2h 3s] and won (836) with two pair, Threes and Deuces
Seat 5: gamerq folded before Flop (didn't bet)
Seat 6: Madsii folded before Flop (didn't bet)
Seat 7: krychuq83th showed [Ac Kh] and lost with a pair of Aces
Seat 8: 2danG4u folded before Flop (didn't bet)
Seat 9: ara_a83 folded before Flop (didn't bet)



PokerStars Hand #165570674302: Tournament #1811877812, $0.91+$0.09 USD Hold'em No Limit - Level II (15/30) - 2017/02/02 20:17:16 CET [2017/02/02 14:17:16 ET]
Table '1811877812 6' 9-max Seat #3 is the button
Seat 2: dyo_live (631 in chips) 
Seat 3: jstoyanov (460 in chips) 
Seat 4: ocsigsxr (836 in chips) 
Seat 5: gamerq (495 in chips) 
Seat 6: Madsii (495 in chips) 
Seat 7: krychuq83th (98 in chips) 
Seat 8: 2danG4u (495 in chips) 
Seat 9: ara_a83 (495 in chips) 
dyo_live: posts the ante 3
jstoyanov: posts the ante 3
ocsigsxr: posts the ante 3
gamerq: posts the ante 3
Madsii: posts the ante 3
krychuq83th: posts the ante 3
2danG4u: posts the ante 3
ara_a83: posts the ante 3
ocsigsxr: posts small blind 15
gamerq: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [2c 9c]
Madsii: folds 
krychuq83th: folds 
2danG4u: folds 
ara_a83: folds 
dyo_live: folds 
jstoyanov: raises 427 to 457 and is all-in
ocsigsxr: folds 
gamerq: calls 427
*** FLOP *** [Qc 8h 6d]
*** TURN *** [Qc 8h 6d] [3d]
*** RIVER *** [Qc 8h 6d 3d] [Jd]
*** SHOW DOWN ***
gamerq: shows [9h 9s] (a pair of Nines)
jstoyanov: shows [Jh Jc] (three of a kind, Jacks)
jstoyanov collected 953 from pot
*** SUMMARY ***
Total pot 953 | Rake 0 
Board [Qc 8h 6d 3d Jd]
Seat 2: dyo_live folded before Flop (didn't bet)
Seat 3: jstoyanov (button) showed [Jh Jc] and won (953) with three of a kind, Jacks
Seat 4: ocsigsxr (small blind) folded before Flop
Seat 5: gamerq (big blind) showed [9h 9s] and lost with a pair of Nines
Seat 6: Madsii folded before Flop (didn't bet)
Seat 7: krychuq83th folded before Flop (didn't bet)
Seat 8: 2danG4u folded before Flop (didn't bet)
Seat 9: ara_a83 folded before Flop (didn't bet)



PokerStars Hand #165570721379: Tournament #1811877812, $0.91+$0.09 USD Hold'em No Limit - Level III (20/40) - 2017/02/02 20:18:03 CET [2017/02/02 14:18:03 ET]
Table '1811877812 6' 9-max Seat #4 is the button
Seat 1: Glintz (1452 in chips) 
Seat 2: dyo_live (628 in chips) 
Seat 3: jstoyanov (953 in chips) 
Seat 4: ocsigsxr (818 in chips) 
Seat 5: gamerq (35 in chips) 
Seat 6: Madsii (492 in chips) 
Seat 7: krychuq83th (95 in chips) 
Seat 8: 2danG4u (492 in chips) 
Seat 9: ara_a83 (492 in chips) 
Glintz: posts the ante 4
dyo_live: posts the ante 4
jstoyanov: posts the ante 4
ocsigsxr: posts the ante 4
gamerq: posts the ante 4
Madsii: posts the ante 4
krychuq83th: posts the ante 4
2danG4u: posts the ante 4
ara_a83: posts the ante 4
gamerq: posts small blind 20
Madsii: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [4d 6h]
krychuq83th: folds 
2danG4u: raises 448 to 488 and is all-in
ara_a83: folds 
Glintz: folds 
dyo_live: folds 
jstoyanov: raises 448 to 936
ocsigsxr: folds 
gamerq: calls 11 and is all-in
Madsii: folds 
Uncalled bet (448) returned to jstoyanov
*** FLOP *** [2d 4c 8h]
*** TURN *** [2d 4c 8h] [Ac]
*** RIVER *** [2d 4c 8h Ac] [4h]
*** SHOW DOWN ***
2danG4u: shows [Js Qd] (a pair of Fours)
jstoyanov: shows [Ts Th] (two pair, Tens and Fours)
jstoyanov collected 923 from side pot
gamerq: shows [8s 9s] (two pair, Eights and Fours)
jstoyanov collected 160 from main pot
2danG4u finished the tournament in 132nd place
gamerq finished the tournament in 133rd place
*** SUMMARY ***
Total pot 1083 Main pot 160. Side pot 923. | Rake 0 
Board [2d 4c 8h Ac 4h]
Seat 1: Glintz folded before Flop (didn't bet)
Seat 2: dyo_live folded before Flop (didn't bet)
Seat 3: jstoyanov showed [Ts Th] and won (1083) with two pair, Tens and Fours
Seat 4: ocsigsxr (button) folded before Flop (didn't bet)
Seat 5: gamerq (small blind) showed [8s 9s] and lost with two pair, Eights and Fours
Seat 6: Madsii (big blind) folded before Flop
Seat 7: krychuq83th folded before Flop (didn't bet)
Seat 8: 2danG4u showed [Js Qd] and lost with a pair of Fours
Seat 9: ara_a83 folded before Flop (didn't bet)



PokerStars Hand #165570772407: Tournament #1811877812, $0.91+$0.09 USD Hold'em No Limit - Level III (20/40) - 2017/02/02 20:18:54 CET [2017/02/02 14:18:54 ET]
Table '1811877812 6' 9-max Seat #6 is the button
Seat 1: Glintz (1448 in chips) 
Seat 2: dyo_live (624 in chips) 
Seat 3: jstoyanov (1544 in chips) 
Seat 4: ocsigsxr (814 in chips) 
Seat 6: Madsii (448 in chips) 
Seat 7: krychuq83th (91 in chips) 
Seat 9: ara_a83 (488 in chips) 
Glintz: posts the ante 4
dyo_live: posts the ante 4
jstoyanov: posts the ante 4
ocsigsxr: posts the ante 4
Madsii: posts the ante 4
krychuq83th: posts the ante 4
ara_a83: posts the ante 4
krychuq83th: posts small blind 20
ara_a83: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [8c 7d]
Glintz: folds 
dyo_live: folds 
jstoyanov: calls 40
ocsigsxr: folds 
Madsii has timed out
Madsii: folds 
Madsii is sitting out
krychuq83th has timed out
krychuq83th: folds 
krychuq83th is sitting out
ara_a83: checks 
*** FLOP *** [5h Th Ah]
krychuq83th has returned
ara_a83: bets 40
jstoyanov: calls 40
*** TURN *** [5h Th Ah] [8d]
ara_a83: bets 104
jstoyanov: calls 104
*** RIVER *** [5h Th Ah 8d] [5c]
ara_a83: bets 300 and is all-in
jstoyanov: folds 
Uncalled bet (300) returned to ara_a83
ara_a83 collected 416 from pot
ara_a83: doesn't show hand 
*** SUMMARY ***
Total pot 416 | Rake 0 
Board [5h Th Ah 8d 5c]
Seat 1: Glintz folded before Flop (didn't bet)
Seat 2: dyo_live folded before Flop (didn't bet)
Seat 3: jstoyanov folded on the River
Seat 4: ocsigsxr folded before Flop (didn't bet)
Seat 6: Madsii (button) folded before Flop (didn't bet)
Seat 7: krychuq83th (small blind) folded before Flop
Seat 9: ara_a83 (big blind) collected (416)



PokerStars Hand #165570861212: Tournament #1811877812, $0.91+$0.09 USD Hold'em No Limit - Level IV (30/60) - 2017/02/02 20:20:24 CET [2017/02/02 14:20:24 ET]
Table '1811877812 6' 9-max Seat #7 is the button
Seat 1: Glintz (1444 in chips) 
Seat 2: dyo_live (620 in chips) 
Seat 3: jstoyanov (1356 in chips) 
Seat 4: ocsigsxr (810 in chips) 
Seat 5: Savat2012 (440 in chips) 
Seat 6: Madsii (444 in chips) is sitting out
Seat 7: krychuq83th (67 in chips) 
Seat 8: mininina187 (563 in chips) out of hand (moved from another table into small blind)
Seat 9: ara_a83 (716 in chips) 
Glintz: posts the ante 6
dyo_live: posts the ante 6
jstoyanov: posts the ante 6
ocsigsxr: posts the ante 6
Savat2012: posts the ante 6
Madsii: posts the ante 6
krychuq83th: posts the ante 6
ara_a83: posts the ante 6
ara_a83: posts small blind 30
Glintz: posts big blind 60
*** HOLE CARDS ***
Dealt to krychuq83th [2c 7d]
Madsii has returned
dyo_live: folds 
jstoyanov: raises 60 to 120
ocsigsxr: folds 
Savat2012: folds 
Madsii: raises 318 to 438 and is all-in
krychuq83th: calls 61 and is all-in
ara_a83: folds 
Glintz: folds 
jstoyanov: calls 318
*** FLOP *** [9d Jd 3c]
*** TURN *** [9d Jd 3c] [9s]
*** RIVER *** [9d Jd 3c 9s] [8d]
*** SHOW DOWN ***
jstoyanov: shows [Kc Js] (two pair, Jacks and Nines)
Madsii: shows [Qh Ah] (a pair of Nines)
jstoyanov collected 754 from side pot
krychuq83th: shows [2c 7d] (a pair of Nines)
jstoyanov collected 321 from main pot
Madsii finished the tournament in 108th place
krychuq83th finished the tournament in 109th place
*** SUMMARY ***
Total pot 1075 Main pot 321. Side pot 754. | Rake 0 
Board [9d Jd 3c 9s 8d]
Seat 1: Glintz (big blind) folded before Flop
Seat 2: dyo_live folded before Flop (didn't bet)
Seat 3: jstoyanov showed [Kc Js] and won (1075) with two pair, Jacks and Nines
Seat 4: ocsigsxr folded before Flop (didn't bet)
Seat 5: Savat2012 folded before Flop (didn't bet)
Seat 6: Madsii showed [Qh Ah] and lost with a pair of Nines
Seat 7: krychuq83th (button) showed [2c 7d] and lost with a pair of Nines
Seat 9: ara_a83 (small blind) folded before Flop



