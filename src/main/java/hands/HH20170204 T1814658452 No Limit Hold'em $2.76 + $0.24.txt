﻿PokerStars Hand #165699676286: Tournament #1814658452, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/05 3:22:01 CET [2017/02/04 21:22:01 ET]
Table '1814658452 1' 3-max Seat #1 is the button
Seat 1: hanney81 (500 in chips) 
Seat 2: krychuq83th (500 in chips) 
Seat 3: ramazan0502 (500 in chips) 
krychuq83th: posts small blind 10
ramazan0502: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [2s 8d]
hanney81: raises 20 to 40
krychuq83th: folds 
ramazan0502: folds 
Uncalled bet (20) returned to hanney81
hanney81 collected 50 from pot
*** SUMMARY ***
Total pot 50 | Rake 0 
Seat 1: hanney81 (button) collected (50)
Seat 2: krychuq83th (small blind) folded before Flop
Seat 3: ramazan0502 (big blind) folded before Flop



PokerStars Hand #165699680586: Tournament #1814658452, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/05 3:22:09 CET [2017/02/04 21:22:09 ET]
Table '1814658452 1' 3-max Seat #2 is the button
Seat 1: hanney81 (530 in chips) 
Seat 2: krychuq83th (490 in chips) 
Seat 3: ramazan0502 (480 in chips) 
ramazan0502: posts small blind 10
hanney81: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [5h Ks]
krychuq83th: folds 
ramazan0502: calls 10
hanney81: checks 
*** FLOP *** [3s 9h 3d]
ramazan0502: checks 
hanney81: bets 20
ramazan0502: calls 20
*** TURN *** [3s 9h 3d] [Kd]
ramazan0502: bets 440 and is all-in
hanney81: calls 440
*** RIVER *** [3s 9h 3d Kd] [4c]
*** SHOW DOWN ***
ramazan0502: shows [Kc 8c] (two pair, Kings and Threes)
hanney81: shows [3c 8d] (three of a kind, Threes)
hanney81 collected 960 from pot
ramazan0502 finished the tournament in 3rd place
*** SUMMARY ***
Total pot 960 | Rake 0 
Board [3s 9h 3d Kd 4c]
Seat 1: hanney81 (big blind) showed [3c 8d] and won (960) with three of a kind, Threes
Seat 2: krychuq83th (button) folded before Flop (didn't bet)
Seat 3: ramazan0502 (small blind) showed [Kc 8c] and lost with two pair, Kings and Threes



PokerStars Hand #165699703154: Tournament #1814658452, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/05 3:22:56 CET [2017/02/04 21:22:56 ET]
Table '1814658452 1' 3-max Seat #1 is the button
Seat 1: hanney81 (1010 in chips) 
Seat 2: krychuq83th (490 in chips) 
hanney81: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [7s 2h]
hanney81: calls 10
krychuq83th: checks 
*** FLOP *** [5s 3h Ts]
krychuq83th: checks 
hanney81: checks 
*** TURN *** [5s 3h Ts] [Ac]
krychuq83th: checks 
hanney81: bets 20
krychuq83th: folds 
Uncalled bet (20) returned to hanney81
hanney81 collected 40 from pot
*** SUMMARY ***
Total pot 40 | Rake 0 
Board [5s 3h Ts Ac]
Seat 1: hanney81 (button) (small blind) collected (40)
Seat 2: krychuq83th (big blind) folded on the Turn



PokerStars Hand #165699715155: Tournament #1814658452, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/05 3:23:20 CET [2017/02/04 21:23:20 ET]
Table '1814658452 1' 3-max Seat #2 is the button
Seat 1: hanney81 (1030 in chips) 
Seat 2: krychuq83th (470 in chips) 
krychuq83th: posts small blind 10
hanney81: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Jc Jd]
krychuq83th: raises 20 to 40
hanney81: folds 
Uncalled bet (20) returned to krychuq83th
krychuq83th collected 40 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 40 | Rake 0 
Seat 1: hanney81 (big blind) folded before Flop
Seat 2: krychuq83th (button) (small blind) collected (40)



PokerStars Hand #165699717167: Tournament #1814658452, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/05 3:23:25 CET [2017/02/04 21:23:25 ET]
Table '1814658452 1' 3-max Seat #1 is the button
Seat 1: hanney81 (1010 in chips) 
Seat 2: krychuq83th (490 in chips) 
hanney81: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Jc 5c]
hanney81: calls 10
krychuq83th: checks 
*** FLOP *** [6s 2s Ac]
krychuq83th: checks 
hanney81: checks 
*** TURN *** [6s 2s Ac] [8c]
krychuq83th: checks 
hanney81: checks 
*** RIVER *** [6s 2s Ac 8c] [7d]
krychuq83th: checks 
hanney81: checks 
*** SHOW DOWN ***
krychuq83th: shows [Jc 5c] (high card Ace)
hanney81: shows [3d 3s] (a pair of Threes)
hanney81 collected 40 from pot
*** SUMMARY ***
Total pot 40 | Rake 0 
Board [6s 2s Ac 8c 7d]
Seat 1: hanney81 (button) (small blind) showed [3d 3s] and won (40) with a pair of Threes
Seat 2: krychuq83th (big blind) showed [Jc 5c] and lost with high card Ace



PokerStars Hand #165699727785: Tournament #1814658452, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/05 3:23:46 CET [2017/02/04 21:23:46 ET]
Table '1814658452 1' 3-max Seat #2 is the button
Seat 1: hanney81 (1030 in chips) 
Seat 2: krychuq83th (470 in chips) 
krychuq83th: posts small blind 10
hanney81: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Th Kd]
krychuq83th: calls 10
hanney81: checks 
*** FLOP *** [4s Ts Ks]
hanney81: checks 
krychuq83th: bets 20
hanney81: calls 20
*** TURN *** [4s Ts Ks] [Qd]
hanney81: checks 
krychuq83th: checks 
*** RIVER *** [4s Ts Ks Qd] [Js]
hanney81: checks 
krychuq83th: checks 
*** SHOW DOWN ***
hanney81: shows [6s Ah] (a flush, King high)
krychuq83th: mucks hand 
hanney81 collected 80 from pot
*** SUMMARY ***
Total pot 80 | Rake 0 
Board [4s Ts Ks Qd Js]
Seat 1: hanney81 (big blind) showed [6s Ah] and won (80) with a flush, King high
Seat 2: krychuq83th (button) (small blind) mucked [Th Kd]



PokerStars Hand #165699740134: Tournament #1814658452, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/05 3:24:11 CET [2017/02/04 21:24:11 ET]
Table '1814658452 1' 3-max Seat #1 is the button
Seat 1: hanney81 (1070 in chips) 
Seat 2: krychuq83th (430 in chips) 
hanney81: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [6d 4c]
hanney81: calls 10
krychuq83th: checks 
*** FLOP *** [3d 5h Ts]
krychuq83th: bets 20
hanney81: calls 20
*** TURN *** [3d 5h Ts] [Jc]
krychuq83th: checks 
hanney81: checks 
*** RIVER *** [3d 5h Ts Jc] [3c]
krychuq83th: checks 
hanney81: checks 
*** SHOW DOWN ***
krychuq83th: shows [6d 4c] (a pair of Threes)
hanney81: shows [Qd Ks] (a pair of Threes - King kicker)
hanney81 collected 80 from pot
*** SUMMARY ***
Total pot 80 | Rake 0 
Board [3d 5h Ts Jc 3c]
Seat 1: hanney81 (button) (small blind) showed [Qd Ks] and won (80) with a pair of Threes
Seat 2: krychuq83th (big blind) showed [6d 4c] and lost with a pair of Threes



PokerStars Hand #165699757545: Tournament #1814658452, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/05 3:24:47 CET [2017/02/04 21:24:47 ET]
Table '1814658452 1' 3-max Seat #2 is the button
Seat 1: hanney81 (1110 in chips) 
Seat 2: krychuq83th (390 in chips) 
krychuq83th: posts small blind 10
hanney81: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [5s Td]
krychuq83th: folds 
Uncalled bet (10) returned to hanney81
hanney81 collected 20 from pot
*** SUMMARY ***
Total pot 20 | Rake 0 
Seat 1: hanney81 (big blind) collected (20)
Seat 2: krychuq83th (button) (small blind) folded before Flop



PokerStars Hand #165699760001: Tournament #1814658452, $2.76+$0.24 USD Hold'em No Limit - Level I (10/20) - 2017/02/05 3:24:52 CET [2017/02/04 21:24:52 ET]
Table '1814658452 1' 3-max Seat #1 is the button
Seat 1: hanney81 (1120 in chips) 
Seat 2: krychuq83th (380 in chips) 
hanney81: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Kd 5c]
hanney81: calls 10
krychuq83th: checks 
*** FLOP *** [Td Jc Ah]
krychuq83th: checks 
hanney81: checks 
*** TURN *** [Td Jc Ah] [8h]
krychuq83th: checks 
hanney81: checks 
*** RIVER *** [Td Jc Ah 8h] [3s]
krychuq83th: checks 
hanney81: checks 
*** SHOW DOWN ***
krychuq83th: shows [Kd 5c] (high card Ace)
hanney81: shows [3c 5h] (a pair of Threes)
hanney81 collected 40 from pot
*** SUMMARY ***
Total pot 40 | Rake 0 
Board [Td Jc Ah 8h 3s]
Seat 1: hanney81 (button) (small blind) showed [3c 5h] and won (40) with a pair of Threes
Seat 2: krychuq83th (big blind) showed [Kd 5c] and lost with high card Ace



PokerStars Hand #165699774435: Tournament #1814658452, $2.76+$0.24 USD Hold'em No Limit - Level II (15/30) - 2017/02/05 3:25:22 CET [2017/02/04 21:25:22 ET]
Table '1814658452 1' 3-max Seat #2 is the button
Seat 1: hanney81 (1140 in chips) 
Seat 2: krychuq83th (360 in chips) 
krychuq83th: posts small blind 15
hanney81: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [4s Jc]
krychuq83th: folds 
Uncalled bet (15) returned to hanney81
hanney81 collected 30 from pot
*** SUMMARY ***
Total pot 30 | Rake 0 
Seat 1: hanney81 (big blind) collected (30)
Seat 2: krychuq83th (button) (small blind) folded before Flop



PokerStars Hand #165699776239: Tournament #1814658452, $2.76+$0.24 USD Hold'em No Limit - Level II (15/30) - 2017/02/05 3:25:26 CET [2017/02/04 21:25:26 ET]
Table '1814658452 1' 3-max Seat #1 is the button
Seat 1: hanney81 (1155 in chips) 
Seat 2: krychuq83th (345 in chips) 
hanney81: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Kd 2d]
hanney81: calls 15
krychuq83th: checks 
*** FLOP *** [3h Js Th]
krychuq83th: checks 
hanney81: checks 
*** TURN *** [3h Js Th] [Kh]
krychuq83th: checks 
hanney81: checks 
*** RIVER *** [3h Js Th Kh] [Qc]
krychuq83th: checks 
hanney81: bets 30
krychuq83th: calls 30
*** SHOW DOWN ***
hanney81: shows [7h As] (a straight, Ten to Ace)
krychuq83th: mucks hand 
hanney81 collected 120 from pot
*** SUMMARY ***
Total pot 120 | Rake 0 
Board [3h Js Th Kh Qc]
Seat 1: hanney81 (button) (small blind) showed [7h As] and won (120) with a straight, Ten to Ace
Seat 2: krychuq83th (big blind) mucked [Kd 2d]



PokerStars Hand #165699783939: Tournament #1814658452, $2.76+$0.24 USD Hold'em No Limit - Level II (15/30) - 2017/02/05 3:25:42 CET [2017/02/04 21:25:42 ET]
Table '1814658452 1' 3-max Seat #2 is the button
Seat 1: hanney81 (1215 in chips) 
Seat 2: krychuq83th (285 in chips) 
krychuq83th: posts small blind 15
hanney81: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [5d 4d]
krychuq83th: raises 255 to 285 and is all-in
hanney81: calls 255
*** FLOP *** [5c 7d Jd]
*** TURN *** [5c 7d Jd] [Js]
*** RIVER *** [5c 7d Jd Js] [Qd]
*** SHOW DOWN ***
hanney81: shows [Ac 5h] (two pair, Jacks and Fives)
krychuq83th: shows [5d 4d] (a flush, Queen high)
krychuq83th collected 570 from pot
*** SUMMARY ***
Total pot 570 | Rake 0 
Board [5c 7d Jd Js Qd]
Seat 1: hanney81 (big blind) showed [Ac 5h] and lost with two pair, Jacks and Fives
Seat 2: krychuq83th (button) (small blind) showed [5d 4d] and won (570) with a flush, Queen high



PokerStars Hand #165699791913: Tournament #1814658452, $2.76+$0.24 USD Hold'em No Limit - Level II (15/30) - 2017/02/05 3:25:58 CET [2017/02/04 21:25:58 ET]
Table '1814658452 1' 3-max Seat #1 is the button
Seat 1: hanney81 (930 in chips) 
Seat 2: krychuq83th (570 in chips) 
hanney81: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Ah Td]
hanney81: calls 15
krychuq83th: raises 540 to 570 and is all-in
hanney81: calls 540
*** FLOP *** [9h 8h As]
*** TURN *** [9h 8h As] [Js]
*** RIVER *** [9h 8h As Js] [8c]
*** SHOW DOWN ***
krychuq83th: shows [Ah Td] (two pair, Aces and Eights)
hanney81: shows [Kd 8s] (three of a kind, Eights)
hanney81 collected 1140 from pot
krychuq83th finished the tournament in 2nd place
hanney81 wins the tournament and receives $18.00 - congratulations!
*** SUMMARY ***
Total pot 1140 | Rake 0 
Board [9h 8h As Js 8c]
Seat 1: hanney81 (button) (small blind) showed [Kd 8s] and won (1140) with three of a kind, Eights
Seat 2: krychuq83th (big blind) showed [Ah Td] and lost with two pair, Aces and Eights



