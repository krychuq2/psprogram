﻿PokerStars Hand #165470813583: Tournament #1809862862, $0.91+$0.09 USD Hold'em No Limit - Level I (10/20) - 2017/01/31 20:58:59 CET [2017/01/31 14:58:59 ET]
Table '1809862862 4' 9-max Seat #1 is the button
Seat 1: Leandro A.37 (1500 in chips) 
Seat 2: kuramsk (1500 in chips) 
Seat 3: wial88 (1500 in chips) 
Seat 4: KoshejSergej (1500 in chips) 
Seat 5: Savat2012 (1500 in chips) 
Seat 6: krychuq83th (1500 in chips) 
Seat 7: VovaVRV (1500 in chips) 
Seat 8: ezzlyy (1500 in chips) 
Seat 9: theo14583 (1500 in chips) 
Leandro A.37: posts the ante 3
kuramsk: posts the ante 3
wial88: posts the ante 3
KoshejSergej: posts the ante 3
Savat2012: posts the ante 3
krychuq83th: posts the ante 3
VovaVRV: posts the ante 3
ezzlyy: posts the ante 3
theo14583: posts the ante 3
kuramsk: posts small blind 10
wial88: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [Kd Jh]
KoshejSergej: calls 20
Savat2012: folds 
krychuq83th: raises 20 to 40
VovaVRV: folds 
ezzlyy: folds 
theo14583: folds 
Leandro A.37: folds 
kuramsk: folds 
wial88: folds 
KoshejSergej: calls 20
*** FLOP *** [Jd 7c Kc]
KoshejSergej: checks 
krychuq83th: bets 40
KoshejSergej: folds 
Uncalled bet (40) returned to krychuq83th
krychuq83th collected 137 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 137 | Rake 0 
Board [Jd 7c Kc]
Seat 1: Leandro A.37 (button) folded before Flop (didn't bet)
Seat 2: kuramsk (small blind) folded before Flop
Seat 3: wial88 (big blind) folded before Flop
Seat 4: KoshejSergej folded on the Flop
Seat 5: Savat2012 folded before Flop (didn't bet)
Seat 6: krychuq83th collected (137)
Seat 7: VovaVRV folded before Flop (didn't bet)
Seat 8: ezzlyy folded before Flop (didn't bet)
Seat 9: theo14583 folded before Flop (didn't bet)



PokerStars Hand #165470865738: Tournament #1809862862, $0.91+$0.09 USD Hold'em No Limit - Level I (10/20) - 2017/01/31 21:00:03 CET [2017/01/31 15:00:03 ET]
Table '1809862862 4' 9-max Seat #2 is the button
Seat 1: Leandro A.37 (1497 in chips) 
Seat 2: kuramsk (1487 in chips) 
Seat 3: wial88 (1477 in chips) 
Seat 4: KoshejSergej (1457 in chips) 
Seat 5: Savat2012 (1497 in chips) 
Seat 6: krychuq83th (1594 in chips) 
Seat 7: VovaVRV (1497 in chips) 
Seat 8: ezzlyy (1497 in chips) 
Seat 9: theo14583 (1497 in chips) 
Leandro A.37: posts the ante 3
kuramsk: posts the ante 3
wial88: posts the ante 3
KoshejSergej: posts the ante 3
Savat2012: posts the ante 3
krychuq83th: posts the ante 3
VovaVRV: posts the ante 3
ezzlyy: posts the ante 3
theo14583: posts the ante 3
wial88: posts small blind 10
KoshejSergej: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [2s Th]
Savat2012: raises 20 to 40
krychuq83th: folds 
VovaVRV: folds 
ezzlyy: folds 
theo14583: folds 
Leandro A.37: folds 
kuramsk: folds 
wial88: calls 30
KoshejSergej: calls 20
*** FLOP *** [2c 9s Ac]
wial88: checks 
KoshejSergej: checks 
Savat2012: bets 74
wial88: folds 
KoshejSergej: calls 74
*** TURN *** [2c 9s Ac] [6h]
KoshejSergej: checks 
Savat2012: checks 
*** RIVER *** [2c 9s Ac 6h] [5c]
KoshejSergej: bets 295
Savat2012: folds 
Uncalled bet (295) returned to KoshejSergej
KoshejSergej collected 295 from pot
*** SUMMARY ***
Total pot 295 | Rake 0 
Board [2c 9s Ac 6h 5c]
Seat 1: Leandro A.37 folded before Flop (didn't bet)
Seat 2: kuramsk (button) folded before Flop (didn't bet)
Seat 3: wial88 (small blind) folded on the Flop
Seat 4: KoshejSergej (big blind) collected (295)
Seat 5: Savat2012 folded on the River
Seat 6: krychuq83th folded before Flop (didn't bet)
Seat 7: VovaVRV folded before Flop (didn't bet)
Seat 8: ezzlyy folded before Flop (didn't bet)
Seat 9: theo14583 folded before Flop (didn't bet)



PokerStars Hand #165470916547: Tournament #1809862862, $0.91+$0.09 USD Hold'em No Limit - Level I (10/20) - 2017/01/31 21:01:03 CET [2017/01/31 15:01:03 ET]
Table '1809862862 4' 9-max Seat #3 is the button
Seat 1: Leandro A.37 (1494 in chips) 
Seat 2: kuramsk (1484 in chips) 
Seat 3: wial88 (1434 in chips) 
Seat 4: KoshejSergej (1635 in chips) 
Seat 5: Savat2012 (1380 in chips) 
Seat 6: krychuq83th (1591 in chips) 
Seat 7: VovaVRV (1494 in chips) 
Seat 8: ezzlyy (1494 in chips) 
Seat 9: theo14583 (1494 in chips) 
Leandro A.37: posts the ante 3
kuramsk: posts the ante 3
wial88: posts the ante 3
KoshejSergej: posts the ante 3
Savat2012: posts the ante 3
krychuq83th: posts the ante 3
VovaVRV: posts the ante 3
ezzlyy: posts the ante 3
theo14583: posts the ante 3
KoshejSergej: posts small blind 10
Savat2012: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [3d Ks]
krychuq83th: folds 
VovaVRV: folds 
ezzlyy: folds 
theo14583: folds 
Leandro A.37: raises 40 to 60
kuramsk: folds 
wial88: folds 
KoshejSergej: folds 
Savat2012: calls 40
*** FLOP *** [Qd 5h 2d]
Savat2012: checks 
Leandro A.37: bets 79
Savat2012: folds 
Uncalled bet (79) returned to Leandro A.37
Leandro A.37 collected 157 from pot
Leandro A.37: doesn't show hand 
*** SUMMARY ***
Total pot 157 | Rake 0 
Board [Qd 5h 2d]
Seat 1: Leandro A.37 collected (157)
Seat 2: kuramsk folded before Flop (didn't bet)
Seat 3: wial88 (button) folded before Flop (didn't bet)
Seat 4: KoshejSergej (small blind) folded before Flop
Seat 5: Savat2012 (big blind) folded on the Flop
Seat 6: krychuq83th folded before Flop (didn't bet)
Seat 7: VovaVRV folded before Flop (didn't bet)
Seat 8: ezzlyy folded before Flop (didn't bet)
Seat 9: theo14583 folded before Flop (didn't bet)



PokerStars Hand #165470984957: Tournament #1809862862, $0.91+$0.09 USD Hold'em No Limit - Level I (10/20) - 2017/01/31 21:02:15 CET [2017/01/31 15:02:15 ET]
Table '1809862862 4' 9-max Seat #4 is the button
Seat 1: Leandro A.37 (1588 in chips) 
Seat 2: kuramsk (1481 in chips) 
Seat 3: wial88 (1431 in chips) 
Seat 4: KoshejSergej (1622 in chips) 
Seat 5: Savat2012 (1317 in chips) 
Seat 6: krychuq83th (1588 in chips) 
Seat 7: VovaVRV (1491 in chips) 
Seat 8: ezzlyy (1491 in chips) 
Seat 9: theo14583 (1491 in chips) 
Leandro A.37: posts the ante 3
kuramsk: posts the ante 3
wial88: posts the ante 3
KoshejSergej: posts the ante 3
Savat2012: posts the ante 3
krychuq83th: posts the ante 3
VovaVRV: posts the ante 3
ezzlyy: posts the ante 3
theo14583: posts the ante 3
Savat2012: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [3h Kc]
VovaVRV: folds 
ezzlyy: raises 40 to 60
theo14583: folds 
Leandro A.37: folds 
kuramsk: raises 120 to 180
wial88: folds 
KoshejSergej: folds 
Savat2012: calls 170
krychuq83th: folds 
ezzlyy: raises 280 to 460
kuramsk: raises 1018 to 1478 and is all-in
Savat2012: folds 
ezzlyy: calls 1018
*** FLOP *** [2s 9s 4h]
*** TURN *** [2s 9s 4h] [2c]
*** RIVER *** [2s 9s 4h 2c] [Jd]
*** SHOW DOWN ***
ezzlyy: shows [Qd Qc] (two pair, Queens and Deuces)
kuramsk: shows [Kd Ad] (a pair of Deuces)
ezzlyy collected 3183 from pot
kuramsk finished the tournament in 81st place
*** SUMMARY ***
Total pot 3183 | Rake 0 
Board [2s 9s 4h 2c Jd]
Seat 1: Leandro A.37 folded before Flop (didn't bet)
Seat 2: kuramsk showed [Kd Ad] and lost with a pair of Deuces
Seat 3: wial88 folded before Flop (didn't bet)
Seat 4: KoshejSergej (button) folded before Flop (didn't bet)
Seat 5: Savat2012 (small blind) folded before Flop
Seat 6: krychuq83th (big blind) folded before Flop
Seat 7: VovaVRV folded before Flop (didn't bet)
Seat 8: ezzlyy showed [Qd Qc] and won (3183) with two pair, Queens and Deuces
Seat 9: theo14583 folded before Flop (didn't bet)



PokerStars Hand #165471056403: Tournament #1809862862, $0.91+$0.09 USD Hold'em No Limit - Level I (10/20) - 2017/01/31 21:03:26 CET [2017/01/31 15:03:26 ET]
Table '1809862862 4' 9-max Seat #5 is the button
Seat 1: Leandro A.37 (1585 in chips) 
Seat 3: wial88 (1428 in chips) 
Seat 4: KoshejSergej (1619 in chips) 
Seat 5: Savat2012 (1134 in chips) 
Seat 6: krychuq83th (1565 in chips) 
Seat 7: VovaVRV (1488 in chips) 
Seat 8: ezzlyy (3193 in chips) 
Seat 9: theo14583 (1488 in chips) 
Leandro A.37: posts the ante 3
wial88: posts the ante 3
KoshejSergej: posts the ante 3
Savat2012: posts the ante 3
krychuq83th: posts the ante 3
VovaVRV: posts the ante 3
ezzlyy: posts the ante 3
theo14583: posts the ante 3
krychuq83th: posts small blind 10
VovaVRV: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [7c 4d]
ezzlyy: folds 
theo14583: raises 20 to 40
Leandro A.37: raises 80 to 120
wial88: folds 
KoshejSergej: folds 
Savat2012: calls 120
krychuq83th: folds 
VovaVRV: folds 
theo14583: folds 
*** FLOP *** [7h 3s 8s]
Leandro A.37: bets 80
Savat2012: calls 80
*** TURN *** [7h 3s 8s] [2s]
Leandro A.37: bets 494
Savat2012: calls 494
*** RIVER *** [7h 3s 8s 2s] [5h]
Leandro A.37: bets 888 and is all-in
Savat2012: folds 
Uncalled bet (888) returned to Leandro A.37
Leandro A.37 collected 1482 from pot
Leandro A.37: doesn't show hand 
*** SUMMARY ***
Total pot 1482 | Rake 0 
Board [7h 3s 8s 2s 5h]
Seat 1: Leandro A.37 collected (1482)
Seat 3: wial88 folded before Flop (didn't bet)
Seat 4: KoshejSergej folded before Flop (didn't bet)
Seat 5: Savat2012 (button) folded on the River
Seat 6: krychuq83th (small blind) folded before Flop
Seat 7: VovaVRV (big blind) folded before Flop
Seat 8: ezzlyy folded before Flop (didn't bet)
Seat 9: theo14583 folded before Flop



PokerStars Hand #165471133503: Tournament #1809862862, $0.91+$0.09 USD Hold'em No Limit - Level II (15/30) - 2017/01/31 21:04:43 CET [2017/01/31 15:04:43 ET]
Table '1809862862 4' 9-max Seat #6 is the button
Seat 1: Leandro A.37 (2370 in chips) 
Seat 3: wial88 (1425 in chips) 
Seat 4: KoshejSergej (1616 in chips) 
Seat 5: Savat2012 (437 in chips) 
Seat 6: krychuq83th (1552 in chips) 
Seat 7: VovaVRV (1465 in chips) 
Seat 8: ezzlyy (3190 in chips) 
Seat 9: theo14583 (1445 in chips) 
Leandro A.37: posts the ante 4
wial88: posts the ante 4
KoshejSergej: posts the ante 4
Savat2012: posts the ante 4
krychuq83th: posts the ante 4
VovaVRV: posts the ante 4
ezzlyy: posts the ante 4
theo14583: posts the ante 4
VovaVRV: posts small blind 15
ezzlyy: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Ts 3c]
theo14583: folds 
Leandro A.37: folds 
wial88: folds 
KoshejSergej: folds 
Savat2012: raises 403 to 433 and is all-in
krychuq83th: folds 
VovaVRV: folds 
ezzlyy: folds 
Uncalled bet (403) returned to Savat2012
Savat2012 collected 107 from pot
*** SUMMARY ***
Total pot 107 | Rake 0 
Seat 1: Leandro A.37 folded before Flop (didn't bet)
Seat 3: wial88 folded before Flop (didn't bet)
Seat 4: KoshejSergej folded before Flop (didn't bet)
Seat 5: Savat2012 collected (107)
Seat 6: krychuq83th (button) folded before Flop (didn't bet)
Seat 7: VovaVRV (small blind) folded before Flop
Seat 8: ezzlyy (big blind) folded before Flop
Seat 9: theo14583 folded before Flop (didn't bet)



PokerStars Hand #165471176334: Tournament #1809862862, $0.91+$0.09 USD Hold'em No Limit - Level II (15/30) - 2017/01/31 21:05:25 CET [2017/01/31 15:05:25 ET]
Table '1809862862 4' 9-max Seat #7 is the button
Seat 1: Leandro A.37 (2366 in chips) 
Seat 3: wial88 (1421 in chips) 
Seat 4: KoshejSergej (1612 in chips) 
Seat 5: Savat2012 (510 in chips) 
Seat 6: krychuq83th (1548 in chips) 
Seat 7: VovaVRV (1446 in chips) 
Seat 8: ezzlyy (3156 in chips) 
Seat 9: theo14583 (1441 in chips) 
Leandro A.37: posts the ante 4
wial88: posts the ante 4
KoshejSergej: posts the ante 4
Savat2012: posts the ante 4
krychuq83th: posts the ante 4
VovaVRV: posts the ante 4
ezzlyy: posts the ante 4
theo14583: posts the ante 4
ezzlyy: posts small blind 15
theo14583: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Qs Tc]
Leandro A.37: folds 
wial88: folds 
KoshejSergej: folds 
Savat2012: folds 
krychuq83th: folds 
VovaVRV: raises 60 to 90
ezzlyy: calls 75
theo14583: folds 
*** FLOP *** [Jh 6h 5d]
ezzlyy: checks 
VovaVRV: checks 
*** TURN *** [Jh 6h 5d] [6s]
ezzlyy: bets 162
VovaVRV: folds 
Uncalled bet (162) returned to ezzlyy
ezzlyy collected 242 from pot
ezzlyy: doesn't show hand 
*** SUMMARY ***
Total pot 242 | Rake 0 
Board [Jh 6h 5d 6s]
Seat 1: Leandro A.37 folded before Flop (didn't bet)
Seat 3: wial88 folded before Flop (didn't bet)
Seat 4: KoshejSergej folded before Flop (didn't bet)
Seat 5: Savat2012 folded before Flop (didn't bet)
Seat 6: krychuq83th folded before Flop (didn't bet)
Seat 7: VovaVRV (button) folded on the Turn
Seat 8: ezzlyy (small blind) collected (242)
Seat 9: theo14583 (big blind) folded before Flop



PokerStars Hand #165471266855: Tournament #1809862862, $0.91+$0.09 USD Hold'em No Limit - Level II (15/30) - 2017/01/31 21:06:54 CET [2017/01/31 15:06:54 ET]
Table '1809862862 4' 9-max Seat #8 is the button
Seat 1: Leandro A.37 (2362 in chips) 
Seat 3: wial88 (1417 in chips) 
Seat 4: KoshejSergej (1608 in chips) 
Seat 5: Savat2012 (506 in chips) 
Seat 6: krychuq83th (1544 in chips) 
Seat 7: VovaVRV (1352 in chips) 
Seat 8: ezzlyy (3304 in chips) 
Seat 9: theo14583 (1407 in chips) 
Leandro A.37: posts the ante 4
wial88: posts the ante 4
KoshejSergej: posts the ante 4
Savat2012: posts the ante 4
krychuq83th: posts the ante 4
VovaVRV: posts the ante 4
ezzlyy: posts the ante 4
theo14583: posts the ante 4
theo14583: posts small blind 15
Leandro A.37: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Td Tc]
wial88: folds 
KoshejSergej: calls 30
Savat2012: folds 
krychuq83th: raises 30 to 60
VovaVRV: folds 
ezzlyy: folds 
theo14583: folds 
Leandro A.37: folds 
KoshejSergej: calls 30
*** FLOP *** [Qh Jc Ah]
KoshejSergej: checks 
krychuq83th: checks 
*** TURN *** [Qh Jc Ah] [5h]
KoshejSergej: checks 
krychuq83th: checks 
*** RIVER *** [Qh Jc Ah 5h] [Ac]
KoshejSergej: checks 
krychuq83th: checks 
*** SHOW DOWN ***
KoshejSergej: shows [4s 4c] (two pair, Aces and Fours)
krychuq83th: shows [Td Tc] (two pair, Aces and Tens)
krychuq83th collected 197 from pot
*** SUMMARY ***
Total pot 197 | Rake 0 
Board [Qh Jc Ah 5h Ac]
Seat 1: Leandro A.37 (big blind) folded before Flop
Seat 3: wial88 folded before Flop (didn't bet)
Seat 4: KoshejSergej showed [4s 4c] and lost with two pair, Aces and Fours
Seat 5: Savat2012 folded before Flop (didn't bet)
Seat 6: krychuq83th showed [Td Tc] and won (197) with two pair, Aces and Tens
Seat 7: VovaVRV folded before Flop (didn't bet)
Seat 8: ezzlyy (button) folded before Flop (didn't bet)
Seat 9: theo14583 (small blind) folded before Flop



PokerStars Hand #165471347517: Tournament #1809862862, $0.91+$0.09 USD Hold'em No Limit - Level II (15/30) - 2017/01/31 21:08:13 CET [2017/01/31 15:08:13 ET]
Table '1809862862 4' 9-max Seat #9 is the button
Seat 1: Leandro A.37 (2328 in chips) 
Seat 2: Yanochechka (4912 in chips) 
Seat 3: wial88 (1413 in chips) 
Seat 4: KoshejSergej (1544 in chips) 
Seat 5: Savat2012 (502 in chips) 
Seat 6: krychuq83th (1677 in chips) 
Seat 7: VovaVRV (1348 in chips) 
Seat 8: ezzlyy (3300 in chips) 
Seat 9: theo14583 (1388 in chips) 
Leandro A.37: posts the ante 4
Yanochechka: posts the ante 4
wial88: posts the ante 4
KoshejSergej: posts the ante 4
Savat2012: posts the ante 4
krychuq83th: posts the ante 4
VovaVRV: posts the ante 4
ezzlyy: posts the ante 4
theo14583: posts the ante 4
Leandro A.37: posts small blind 15
Yanochechka: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Ts 8s]
wial88: folds 
KoshejSergej: calls 30
Savat2012: calls 30
krychuq83th: folds 
VovaVRV: folds 
ezzlyy: raises 120 to 150
theo14583: folds 
Leandro A.37: folds 
Yanochechka: folds 
KoshejSergej: calls 120
Savat2012: folds 
*** FLOP *** [5s 7h Qc]
KoshejSergej: checks 
ezzlyy: bets 221
KoshejSergej: raises 1169 to 1390 and is all-in
ezzlyy: calls 1169
*** TURN *** [5s 7h Qc] [Jd]
*** RIVER *** [5s 7h Qc Jd] [Ad]
*** SHOW DOWN ***
KoshejSergej: shows [Ks Ac] (a pair of Aces)
ezzlyy: shows [As Qs] (two pair, Aces and Queens)
ezzlyy collected 3191 from pot
KoshejSergej finished the tournament in 69th place
*** SUMMARY ***
Total pot 3191 | Rake 0 
Board [5s 7h Qc Jd Ad]
Seat 1: Leandro A.37 (small blind) folded before Flop
Seat 2: Yanochechka (big blind) folded before Flop
Seat 3: wial88 folded before Flop (didn't bet)
Seat 4: KoshejSergej showed [Ks Ac] and lost with a pair of Aces
Seat 5: Savat2012 folded before Flop
Seat 6: krychuq83th folded before Flop (didn't bet)
Seat 7: VovaVRV folded before Flop (didn't bet)
Seat 8: ezzlyy showed [As Qs] and won (3191) with two pair, Aces and Queens
Seat 9: theo14583 (button) folded before Flop (didn't bet)



PokerStars Hand #165471447869: Tournament #1809862862, $0.91+$0.09 USD Hold'em No Limit - Level III (25/50) - 2017/01/31 21:09:53 CET [2017/01/31 15:09:53 ET]
Table '1809862862 4' 9-max Seat #1 is the button
Seat 1: Leandro A.37 (2309 in chips) 
Seat 2: Yanochechka (4878 in chips) 
Seat 3: wial88 (1409 in chips) 
Seat 5: Savat2012 (468 in chips) 
Seat 6: krychuq83th (1673 in chips) 
Seat 7: VovaVRV (1344 in chips) 
Seat 8: ezzlyy (4947 in chips) 
Seat 9: theo14583 (1384 in chips) 
Leandro A.37: posts the ante 6
Yanochechka: posts the ante 6
wial88: posts the ante 6
Savat2012: posts the ante 6
krychuq83th: posts the ante 6
VovaVRV: posts the ante 6
ezzlyy: posts the ante 6
theo14583: posts the ante 6
Yanochechka: posts small blind 25
wial88: posts big blind 50
*** HOLE CARDS ***
Dealt to krychuq83th [3c 8c]
Savat2012: raises 412 to 462 and is all-in
krychuq83th: folds 
VovaVRV: folds 
ezzlyy: calls 462
theo14583: folds 
Leandro A.37: folds 
Yanochechka: calls 437
wial88: folds 
*** FLOP *** [4s Js 8h]
Yanochechka: bets 4410 and is all-in
ezzlyy: folds 
Uncalled bet (4410) returned to Yanochechka
*** TURN *** [4s Js 8h] [4c]
*** RIVER *** [4s Js 8h 4c] [Qd]
*** SHOW DOWN ***
Yanochechka: shows [2d Jd] (two pair, Jacks and Fours)
Savat2012: shows [Kc Jc] (two pair, Jacks and Fours - King kicker)
Savat2012 collected 1484 from pot
*** SUMMARY ***
Total pot 1484 | Rake 0 
Board [4s Js 8h 4c Qd]
Seat 1: Leandro A.37 (button) folded before Flop (didn't bet)
Seat 2: Yanochechka (small blind) showed [2d Jd] and lost with two pair, Jacks and Fours
Seat 3: wial88 (big blind) folded before Flop
Seat 5: Savat2012 showed [Kc Jc] and won (1484) with two pair, Jacks and Fours
Seat 6: krychuq83th folded before Flop (didn't bet)
Seat 7: VovaVRV folded before Flop (didn't bet)
Seat 8: ezzlyy folded on the Flop
Seat 9: theo14583 folded before Flop (didn't bet)



PokerStars Hand #165471529231: Tournament #1809862862, $0.91+$0.09 USD Hold'em No Limit - Level III (25/50) - 2017/01/31 21:11:13 CET [2017/01/31 15:11:13 ET]
Table '1809862862 4' 9-max Seat #2 is the button
Seat 1: Leandro A.37 (2303 in chips) 
Seat 2: Yanochechka (4410 in chips) 
Seat 3: wial88 (1353 in chips) 
Seat 5: Savat2012 (1484 in chips) 
Seat 6: krychuq83th (1667 in chips) 
Seat 7: VovaVRV (1338 in chips) 
Seat 8: ezzlyy (4479 in chips) 
Seat 9: theo14583 (1378 in chips) 
Leandro A.37: posts the ante 6
Yanochechka: posts the ante 6
wial88: posts the ante 6
Savat2012: posts the ante 6
krychuq83th: posts the ante 6
VovaVRV: posts the ante 6
ezzlyy: posts the ante 6
theo14583: posts the ante 6
wial88: posts small blind 25
Savat2012: posts big blind 50
*** HOLE CARDS ***
Dealt to krychuq83th [2c Jh]
krychuq83th: folds 
VovaVRV: folds 
ezzlyy: raises 50 to 100
theo14583: calls 100
Leandro A.37: folds 
Yanochechka: folds 
wial88: folds 
Savat2012: folds 
*** FLOP *** [8h 4d Ts]
ezzlyy: checks 
theo14583: checks 
*** TURN *** [8h 4d Ts] [Ks]
ezzlyy: bets 174
theo14583: raises 276 to 450
ezzlyy: calls 276
*** RIVER *** [8h 4d Ts Ks] [Js]
ezzlyy: checks 
theo14583: bets 450
ezzlyy: folds 
Uncalled bet (450) returned to theo14583
theo14583 collected 1223 from pot
*** SUMMARY ***
Total pot 1223 | Rake 0 
Board [8h 4d Ts Ks Js]
Seat 1: Leandro A.37 folded before Flop (didn't bet)
Seat 2: Yanochechka (button) folded before Flop (didn't bet)
Seat 3: wial88 (small blind) folded before Flop
Seat 5: Savat2012 (big blind) folded before Flop
Seat 6: krychuq83th folded before Flop (didn't bet)
Seat 7: VovaVRV folded before Flop (didn't bet)
Seat 8: ezzlyy folded on the River
Seat 9: theo14583 collected (1223)



PokerStars Hand #165471612959: Tournament #1809862862, $0.91+$0.09 USD Hold'em No Limit - Level III (25/50) - 2017/01/31 21:12:36 CET [2017/01/31 15:12:36 ET]
Table '1809862862 4' 9-max Seat #3 is the button
Seat 1: Leandro A.37 (2297 in chips) 
Seat 2: Yanochechka (4404 in chips) 
Seat 3: wial88 (1322 in chips) 
Seat 5: Savat2012 (1428 in chips) 
Seat 6: krychuq83th (1661 in chips) 
Seat 7: VovaVRV (1332 in chips) 
Seat 8: ezzlyy (3923 in chips) 
Seat 9: theo14583 (2045 in chips) 
Leandro A.37: posts the ante 6
Yanochechka: posts the ante 6
wial88: posts the ante 6
Savat2012: posts the ante 6
krychuq83th: posts the ante 6
VovaVRV: posts the ante 6
ezzlyy: posts the ante 6
theo14583: posts the ante 6
Savat2012: posts small blind 25
krychuq83th: posts big blind 50
*** HOLE CARDS ***
Dealt to krychuq83th [Kh Ks]
VovaVRV: folds 
ezzlyy: folds 
theo14583: folds 
Leandro A.37: folds 
Yanochechka: calls 50
wial88: raises 100 to 150
Savat2012: folds 
krychuq83th: raises 208 to 358
Yanochechka: calls 308
wial88: calls 208
*** FLOP *** [9s 7h Ts]
krychuq83th: bets 687
Yanochechka: calls 687
wial88: raises 271 to 958 and is all-in
krychuq83th: calls 271
Yanochechka: calls 271
*** TURN *** [9s 7h Ts] [5d]
krychuq83th: bets 339 and is all-in
Yanochechka: calls 339
*** RIVER *** [9s 7h Ts 5d] [6d]
*** SHOW DOWN ***
krychuq83th: shows [Kh Ks] (a pair of Kings)
Yanochechka: shows [Kc 8s] (a straight, Six to Ten)
Yanochechka collected 678 from side pot
wial88: shows [Qh Th] (a pair of Tens)
Yanochechka collected 4021 from main pot
krychuq83th finished the tournament in 65th place
wial88 finished the tournament in 66th place
*** SUMMARY ***
Total pot 4699 Main pot 4021. Side pot 678. | Rake 0 
Board [9s 7h Ts 5d 6d]
Seat 1: Leandro A.37 folded before Flop (didn't bet)
Seat 2: Yanochechka showed [Kc 8s] and won (4699) with a straight, Six to Ten
Seat 3: wial88 (button) showed [Qh Th] and lost with a pair of Tens
Seat 5: Savat2012 (small blind) folded before Flop
Seat 6: krychuq83th (big blind) showed [Kh Ks] and lost with a pair of Kings
Seat 7: VovaVRV folded before Flop (didn't bet)
Seat 8: ezzlyy folded before Flop (didn't bet)
Seat 9: theo14583 folded before Flop (didn't bet)



