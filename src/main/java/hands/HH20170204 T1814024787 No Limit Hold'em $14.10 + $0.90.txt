﻿PokerStars Hand #165668268149: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:47:09 CET [2017/02/04 10:47:09 ET]
Table '1814024787 1' 3-max Seat #1 is the button
Seat 1: hasi11916 (500 in chips) 
Seat 2: krychuq83th (500 in chips) 
Seat 3: Antonio9119 (500 in chips) 
krychuq83th: posts small blind 10
Antonio9119: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [5s 4c]
hasi11916: raises 50 to 70
krychuq83th: folds 
Antonio9119: folds 
Uncalled bet (50) returned to hasi11916
hasi11916 collected 50 from pot
hasi11916: doesn't show hand 
*** SUMMARY ***
Total pot 50 | Rake 0 
Seat 1: hasi11916 (button) collected (50)
Seat 2: krychuq83th (small blind) folded before Flop
Seat 3: Antonio9119 (big blind) folded before Flop



PokerStars Hand #165668280315: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:47:24 CET [2017/02/04 10:47:24 ET]
Table '1814024787 1' 3-max Seat #2 is the button
Seat 1: hasi11916 (530 in chips) 
Seat 2: krychuq83th (490 in chips) 
Seat 3: Antonio9119 (480 in chips) 
Antonio9119: posts small blind 10
hasi11916: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [5d 5h]
krychuq83th: raises 40 to 60
Antonio9119: raises 40 to 100
hasi11916: calls 80
krychuq83th: calls 40
*** FLOP *** [6d 7c 2h]
Antonio9119: bets 20
hasi11916: calls 20
krychuq83th: calls 20
*** TURN *** [6d 7c 2h] [4s]
Antonio9119: bets 20
hasi11916: raises 40 to 60
krychuq83th: calls 60
Antonio9119: calls 40
*** RIVER *** [6d 7c 2h 4s] [Th]
Antonio9119: checks 
hasi11916: bets 60
krychuq83th: calls 60
Antonio9119: folds 
*** SHOW DOWN ***
hasi11916: shows [Kc Qs] (high card King)
krychuq83th: shows [5d 5h] (a pair of Fives)
krychuq83th collected 660 from pot
*** SUMMARY ***
Total pot 660 | Rake 0 
Board [6d 7c 2h 4s Th]
Seat 1: hasi11916 (big blind) showed [Kc Qs] and lost with high card King
Seat 2: krychuq83th (button) showed [5d 5h] and won (660) with a pair of Fives
Seat 3: Antonio9119 (small blind) folded on the River



PokerStars Hand #165668330359: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:48:26 CET [2017/02/04 10:48:26 ET]
Table '1814024787 1' 3-max Seat #3 is the button
Seat 1: hasi11916 (290 in chips) 
Seat 2: krychuq83th (910 in chips) 
Seat 3: Antonio9119 (300 in chips) 
hasi11916: posts small blind 10
krychuq83th: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [9h Qs]
Antonio9119 has timed out
Antonio9119: folds 
Antonio9119 is sitting out
hasi11916: calls 10
Antonio9119 has returned
krychuq83th: checks 
*** FLOP *** [7h Jh 8h]
hasi11916: bets 20
krychuq83th: calls 20
*** TURN *** [7h Jh 8h] [8d]
hasi11916: bets 40
krychuq83th: calls 40
*** RIVER *** [7h Jh 8h 8d] [Ad]
hasi11916: bets 80
krychuq83th: folds 
Uncalled bet (80) returned to hasi11916
hasi11916 collected 160 from pot
hasi11916: doesn't show hand 
*** SUMMARY ***
Total pot 160 | Rake 0 
Board [7h Jh 8h 8d Ad]
Seat 1: hasi11916 (small blind) collected (160)
Seat 2: krychuq83th (big blind) folded on the River
Seat 3: Antonio9119 (button) folded before Flop (didn't bet)



PokerStars Hand #165668384094: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:49:33 CET [2017/02/04 10:49:33 ET]
Table '1814024787 1' 3-max Seat #1 is the button
Seat 1: hasi11916 (370 in chips) 
Seat 2: krychuq83th (830 in chips) 
Seat 3: Antonio9119 (300 in chips) 
krychuq83th: posts small blind 10
Antonio9119: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [9h Ts]
hasi11916: raises 50 to 70
krychuq83th: folds 
Antonio9119: folds 
Uncalled bet (50) returned to hasi11916
hasi11916 collected 50 from pot
hasi11916: doesn't show hand 
*** SUMMARY ***
Total pot 50 | Rake 0 
Seat 1: hasi11916 (button) collected (50)
Seat 2: krychuq83th (small blind) folded before Flop
Seat 3: Antonio9119 (big blind) folded before Flop



PokerStars Hand #165668401499: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level I (10/20) - 2017/02/04 16:49:54 CET [2017/02/04 10:49:54 ET]
Table '1814024787 1' 3-max Seat #2 is the button
Seat 1: hasi11916 (400 in chips) 
Seat 2: krychuq83th (820 in chips) 
Seat 3: Antonio9119 (280 in chips) 
Antonio9119: posts small blind 10
hasi11916: posts big blind 20
*** HOLE CARDS ***
Dealt to krychuq83th [9s Qd]
krychuq83th: folds 
Antonio9119: calls 10
hasi11916: raises 40 to 60
Antonio9119: calls 40
*** FLOP *** [5s 8d 6h]
Antonio9119: checks 
hasi11916: checks 
*** TURN *** [5s 8d 6h] [5c]
Antonio9119: checks 
hasi11916: checks 
*** RIVER *** [5s 8d 6h 5c] [8h]
Antonio9119: checks 
hasi11916: checks 
*** SHOW DOWN ***
Antonio9119: shows [4d Qh] (two pair, Eights and Fives)
hasi11916: shows [Ah Jd] (two pair, Eights and Fives - Ace kicker)
hasi11916 collected 120 from pot
*** SUMMARY ***
Total pot 120 | Rake 0 
Board [5s 8d 6h 5c 8h]
Seat 1: hasi11916 (big blind) showed [Ah Jd] and won (120) with two pair, Eights and Fives
Seat 2: krychuq83th (button) folded before Flop (didn't bet)
Seat 3: Antonio9119 (small blind) showed [4d Qh] and lost with two pair, Eights and Fives



PokerStars Hand #165668440090: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 16:50:42 CET [2017/02/04 10:50:42 ET]
Table '1814024787 1' 3-max Seat #3 is the button
Seat 1: hasi11916 (460 in chips) 
Seat 2: krychuq83th (820 in chips) 
Seat 3: Antonio9119 (220 in chips) 
hasi11916: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Qs 6d]
Antonio9119: calls 30
hasi11916: raises 90 to 120
krychuq83th: folds 
Antonio9119: calls 90
*** FLOP *** [5d 9d 9c]
hasi11916: checks 
Antonio9119: checks 
*** TURN *** [5d 9d 9c] [5c]
hasi11916: checks 
Antonio9119: checks 
*** RIVER *** [5d 9d 9c 5c] [3s]
hasi11916: bets 270
Antonio9119: folds 
Uncalled bet (270) returned to hasi11916
hasi11916 collected 270 from pot
hasi11916: doesn't show hand 
*** SUMMARY ***
Total pot 270 | Rake 0 
Board [5d 9d 9c 5c 3s]
Seat 1: hasi11916 (small blind) collected (270)
Seat 2: krychuq83th (big blind) folded before Flop
Seat 3: Antonio9119 (button) folded on the River



PokerStars Hand #165668468797: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 16:51:18 CET [2017/02/04 10:51:18 ET]
Table '1814024787 1' 3-max Seat #1 is the button
Seat 1: hasi11916 (610 in chips) 
Seat 2: krychuq83th (790 in chips) 
Seat 3: Antonio9119 (100 in chips) 
krychuq83th: posts small blind 15
Antonio9119: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [4d 4c]
hasi11916: calls 30
krychuq83th: raises 120 to 150
Antonio9119: folds 
hasi11916: calls 120
*** FLOP *** [Ks Kh Jh]
krychuq83th: bets 165
hasi11916: folds [7h 7d]
Uncalled bet (165) returned to krychuq83th
krychuq83th collected 330 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 330 | Rake 0 
Board [Ks Kh Jh]
Seat 1: hasi11916 (button) folded on the Flop
Seat 2: krychuq83th (small blind) collected (330)
Seat 3: Antonio9119 (big blind) folded before Flop



PokerStars Hand #165668485582: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 16:51:39 CET [2017/02/04 10:51:39 ET]
Table '1814024787 1' 3-max Seat #2 is the button
Seat 1: hasi11916 (460 in chips) 
Seat 2: krychuq83th (970 in chips) 
Seat 3: Antonio9119 (70 in chips) 
Antonio9119: posts small blind 15
hasi11916: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [Td 6d]
krychuq83th has timed out
krychuq83th: folds 
krychuq83th is sitting out
krychuq83th has returned
Antonio9119: raises 40 to 70 and is all-in
hasi11916: calls 40
*** FLOP *** [2s 5h 7s]
*** TURN *** [2s 5h 7s] [Th]
*** RIVER *** [2s 5h 7s Th] [Qh]
*** SHOW DOWN ***
Antonio9119: shows [Kd 7c] (a pair of Sevens)
hasi11916: shows [Qs 8h] (a pair of Queens)
hasi11916 collected 140 from pot
Antonio9119 finished the tournament in 3rd place
*** SUMMARY ***
Total pot 140 | Rake 0 
Board [2s 5h 7s Th Qh]
Seat 1: hasi11916 (big blind) showed [Qs 8h] and won (140) with a pair of Queens
Seat 2: krychuq83th (button) folded before Flop (didn't bet)
Seat 3: Antonio9119 (small blind) showed [Kd 7c] and lost with a pair of Sevens



PokerStars Hand #165668515076: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 16:52:16 CET [2017/02/04 10:52:16 ET]
Table '1814024787 1' 3-max Seat #1 is the button
Seat 1: hasi11916 (530 in chips) 
Seat 2: krychuq83th (970 in chips) 
hasi11916: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [4d Ad]
hasi11916: raises 30 to 60
krychuq83th: raises 910 to 970 and is all-in
hasi11916: folds 
Uncalled bet (910) returned to krychuq83th
krychuq83th collected 120 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 120 | Rake 0 
Seat 1: hasi11916 (button) (small blind) folded before Flop
Seat 2: krychuq83th (big blind) collected (120)



PokerStars Hand #165668521838: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 16:52:24 CET [2017/02/04 10:52:24 ET]
Table '1814024787 1' 3-max Seat #2 is the button
Seat 1: hasi11916 (470 in chips) 
Seat 2: krychuq83th (1030 in chips) 
krychuq83th: posts small blind 15
hasi11916: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [9c 2d]
krychuq83th: folds 
Uncalled bet (15) returned to hasi11916
hasi11916 collected 30 from pot
hasi11916: doesn't show hand 
*** SUMMARY ***
Total pot 30 | Rake 0 
Seat 1: hasi11916 (big blind) collected (30)
Seat 2: krychuq83th (button) (small blind) folded before Flop



PokerStars Hand #165668526219: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 16:52:29 CET [2017/02/04 10:52:29 ET]
Table '1814024787 1' 3-max Seat #1 is the button
Seat 1: hasi11916 (485 in chips) 
Seat 2: krychuq83th (1015 in chips) 
hasi11916: posts small blind 15
krychuq83th: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [9h 8h]
hasi11916: calls 15
krychuq83th: checks 
*** FLOP *** [Tc 7h Qs]
krychuq83th: checks 
hasi11916: bets 60
krychuq83th: calls 60
*** TURN *** [Tc 7h Qs] [6c]
krychuq83th: checks 
hasi11916: bets 90
krychuq83th: raises 90 to 180
hasi11916: folds 
Uncalled bet (90) returned to krychuq83th
krychuq83th collected 360 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 360 | Rake 0 
Board [Tc 7h Qs 6c]
Seat 1: hasi11916 (button) (small blind) folded on the Turn
Seat 2: krychuq83th (big blind) collected (360)



PokerStars Hand #165668553390: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level II (15/30) - 2017/02/04 16:53:03 CET [2017/02/04 10:53:03 ET]
Table '1814024787 1' 3-max Seat #2 is the button
Seat 1: hasi11916 (305 in chips) 
Seat 2: krychuq83th (1195 in chips) 
krychuq83th: posts small blind 15
hasi11916: posts big blind 30
*** HOLE CARDS ***
Dealt to krychuq83th [3h Ad]
krychuq83th: raises 1165 to 1195 and is all-in
hasi11916: folds 
Uncalled bet (1165) returned to krychuq83th
krychuq83th collected 60 from pot
krychuq83th: doesn't show hand 
*** SUMMARY ***
Total pot 60 | Rake 0 
Seat 1: hasi11916 (big blind) folded before Flop
Seat 2: krychuq83th (button) (small blind) collected (60)



PokerStars Hand #165668562173: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 16:53:14 CET [2017/02/04 10:53:14 ET]
Table '1814024787 1' 3-max Seat #1 is the button
Seat 1: hasi11916 (275 in chips) 
Seat 2: krychuq83th (1225 in chips) 
hasi11916: posts small blind 20
krychuq83th: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [Qd 9s]
hasi11916: raises 40 to 80
krychuq83th: folds 
Uncalled bet (40) returned to hasi11916
hasi11916 collected 80 from pot
hasi11916: doesn't show hand 
*** SUMMARY ***
Total pot 80 | Rake 0 
Seat 1: hasi11916 (button) (small blind) collected (80)
Seat 2: krychuq83th (big blind) folded before Flop



PokerStars Hand #165668566577: Tournament #1814024787, $14.10+$0.90 USD Hold'em No Limit - Level III (20/40) - 2017/02/04 16:53:20 CET [2017/02/04 10:53:20 ET]
Table '1814024787 1' 3-max Seat #2 is the button
Seat 1: hasi11916 (315 in chips) 
Seat 2: krychuq83th (1185 in chips) 
krychuq83th: posts small blind 20
hasi11916: posts big blind 40
*** HOLE CARDS ***
Dealt to krychuq83th [Td Kc]
krychuq83th: raises 1145 to 1185 and is all-in
hasi11916: calls 275 and is all-in
Uncalled bet (870) returned to krychuq83th
*** FLOP *** [2c Ah 7c]
*** TURN *** [2c Ah 7c] [Kd]
*** RIVER *** [2c Ah 7c Kd] [6c]
*** SHOW DOWN ***
hasi11916: shows [3s Qs] (high card Ace)
krychuq83th: shows [Td Kc] (a pair of Kings)
krychuq83th collected 630 from pot
hasi11916 finished the tournament in 2nd place
krychuq83th wins the tournament and receives $30.00 - congratulations!
*** SUMMARY ***
Total pot 630 | Rake 0 
Board [2c Ah 7c Kd 6c]
Seat 1: hasi11916 (big blind) showed [3s Qs] and lost with high card Ace
Seat 2: krychuq83th (button) (small blind) showed [Td Kc] and won (630) with a pair of Kings



