package sample.models;

import java.util.ArrayList;
import java.util.List;

import static jdk.nashorn.internal.objects.NativeMath.round;

/**
 * Created by krysn on 21.03.2017.
 */
public final class User {
    static User user;

     public static User valueOf(int Id, String login, String password, String path, Double balance){

         user = new User(Id, login, password, path ,balance);
         return user;
     }
     public static  User getUser(){
       return user;
     };

    private User(int Id, String login, String password, String path, Double balance) {
        this.Id = Id;
        this.login = login;
        this.password = password;
        this.path = path;
        this.balance = balance;
        this.tournaments = tournaments;

    }
    private User(String login, String password, String path, Double balance, ArrayList<Tournament> tournaments) {
        this.login = login;
        this.password = password;
        this.path = path;
        this.balance = balance;
        this.tournaments = tournaments;
    }
   public void updateBalance(double amount){
        this.balance = this.balance + amount;

   }
   public void setBalance(Double balance){
       this.balance = balance;
   }
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }
    public Double getBalance(){
       balance = Math.round(balance*100.0)/100.0;
       return balance;
   }
    public void setPassword(String password) {
        this.password = password;
    }
    public int getId(){

        return Id;
    }
    public String getPath() {
        return path;
    }

    public void setTournaments(List<Tournament> tournaments){
        this.tournaments = tournaments;
    }
    public void addTournament(Tournament tournament){
        tournaments.add(tournament);
    }
    public List<Tournament> getTournaments(){
        return tournaments;
    }

    public void setPath(String path) {
        this.path = path;
    }

    private String login;
    private String password;
    private String path;
    private Double balance;
    private int Id;
    private List<Tournament> tournaments;
}
