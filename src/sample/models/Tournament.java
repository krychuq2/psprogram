package sample.models;

import java.util.Date;

/**
 * Created by krysn on 28.01.2017.
 */
public class Tournament {


  public  String id;
  public   double cost;

  public   String date;
   // ArrayList<Hand> hands;

    public Tournament(String id, double cost, String date){
        this.id = id;
        this.cost = cost;
        this.date = date;
       // this.hands = hands;
    }

//    public Tournament(String id, double cost, Date date){
//        this.id = id;
//        this.cost = cost;
//        this.date = date;
//    }

   public Tournament(){

   }
    public String getId() {
        return id;
    }

    public double getCost() {
        return cost;
    }

    public String getDate() {
        return date;
    }

    //public  ArrayList<Hand> getHands(){
       //return hands;
   // }
}
