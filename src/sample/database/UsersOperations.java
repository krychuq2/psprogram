package sample.database;

import sample.models.User;

import java.sql.*;

import static jdk.nashorn.internal.objects.NativeMath.round;

/**
 * Created by krysn on 21.05.2017.
 */
public class UsersOperations {

    private DataBaseConnection dataBaseConnection = DataBaseConnection.getInstance();
    private Connection connection = dataBaseConnection.getConnection();
    private String path;
    //private User user = User.getUser();

    public UsersOperations(){

    }

    public Boolean registerUser(User user) {
        try {

            path = user.getPath();
            String newPath = path.replace('\\', '/');

            Statement statement = connection.createStatement();
            double balance = Math.round(user.getBalance()*100.0)/100.0;
            String sql = "INSERT INTO user(login, password, path, balance) VALUES ('" + user.getLogin() + "','" + user.getPassword() + "','" + newPath + "','" +balance + "')";

            System.out.println(sql);
            statement.executeUpdate(sql);
            return login(user.getLogin(),user.getPassword());

        } catch (SQLException e) {
            e.printStackTrace();

        }

        return true;
    }

    public Boolean checkLogin(String login) {
        final String queryCheck = "SELECT * from user WHERE login = ?";


        try {
            final PreparedStatement ps = connection.prepareStatement(queryCheck);
            ps.setString(1, login);
            final ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public Boolean login(String login, String password) {
        final String queryLogin = "SELECT * from user WHERE login = ? AND password = ?";


        try {
            final PreparedStatement ps = connection.prepareStatement(queryLogin);
            ps.setString(1, login);
            ps.setString(2, password);
            final ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {

                int userId = resultSet.getInt("id");
                path = resultSet.getString("path");
                String userLogin = resultSet.getString("login");
                String userPassword = resultSet.getString("password");
                Double userBalance = resultSet.getDouble("balance");

                User.valueOf(userId,userLogin,userPassword,path,userBalance);

                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }


    }



}
