package sample.database;


import sample.models.Tournament;
import sample.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static jdk.nashorn.internal.objects.NativeMath.round;

/**
 * Created by krysn on 14.03.2017.
 */
public class TournamentOperations {
    String id  = null;
    String path = null;
    private DataBaseConnection dataBaseConnection = DataBaseConnection.getInstance();
    private Connection connection = dataBaseConnection.getConnection();


    public TournamentOperations() {

    }
    public  Boolean addTournamentCost(Double amount, int userId){
        System.out.println("we are going to add cost "+ amount + " " + userId);
        final String queryLogin = "UPDATE  pokerAi.user set balance = balance + ? WHERE id = ?";
        double cost = Math.round(amount*100.0)/100.0;
        try {
            final PreparedStatement ps = connection.prepareStatement(queryLogin);
            ps.setDouble(1, cost);
            ps.setInt(2,userId);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Boolean insertTournaments(Tournament tournament,String userId) throws SQLException {
        try {

            Statement statement = connection.createStatement();

            String sql = "INSERT INTO tournaments(idTournaments,cost,date,userId) VALUES ('" + tournament.id + "','" + tournament.cost + "','" + tournament.date + "','" + userId +"')";

            System.out.println(sql);
            statement.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;

    }


    public List<Tournament> getTournaments(String userId) {
        List<Tournament>tournaments = new ArrayList<>();

        final String queryLogin = "SELECT * from tournaments WHERE userId = ? ";

        try {
            final PreparedStatement ps = connection.prepareStatement(queryLogin);
            ps.setString(1, userId);
            final ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {

                tournaments.add(new Tournament(resultSet.getString(1), resultSet.getInt(2), resultSet.getString(3)));
            }


        }catch (SQLException e) {
            e.printStackTrace();
        }

        return  tournaments;

    }


//    public Double getBalance(int userId) {
//        final String queryBalanca = "SELECT balance from pokerAi.user WHERE id = ? ";
//
//        try {
//            final PreparedStatement ps = connection.prepareStatement(queryBalanca);
//            ps.setInt(1, userId);
//            final ResultSet resultSet = ps.executeQuery();
//            double balance = 0.0;
//            while (resultSet.next()){
//                 balance = resultSet.getDouble(1);
//
//            }
//            return  balance;
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//            return 0.0;
//
//        }
//
//    }
}