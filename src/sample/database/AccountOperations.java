package sample.database;

import sample.models.User;

import javax.jws.soap.SOAPBinding;
import java.sql.*;
import java.util.*;

import static jdk.nashorn.internal.objects.NativeMath.round;

/**
 * Created by krysn on 29.03.2017.
 */
public class AccountOperations {
    private DataBaseConnection dataBaseConnection = DataBaseConnection.getInstance();
    private Connection connection = dataBaseConnection.getConnection();



    public AccountOperations(){

    }

    public  Boolean deposit(int amount){
        java.sql.Date sqlDate = new java.sql.Date(Calendar.getInstance().getTimeInMillis());

        try {
            Statement statement = connection.createStatement();
            final String queryCheck = "INSERT INTO accountOperation(amount, time, userId ) VALUES(?,?,?)";
            final PreparedStatement ps = connection.prepareStatement(queryCheck);
            ps.setInt(1, amount);
            ps.setDate(2, sqlDate);
            ps.setInt(3, User.getUser().getId());
            ps.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        //HomeController.updateWalletText();

        //  user.updateBalance(amount);
       // System.out.println("amount "+"$" + amount + "user id is " + user.getId());
    }

    public Double getBalance(int userId) {
        final String queryBalanca = "SELECT balance from pokerAi.user WHERE id = ? ";

        try {
            final PreparedStatement ps = connection.prepareStatement(queryBalanca);
            ps.setInt(1, userId);
            final ResultSet resultSet = ps.executeQuery();
            double balance = 0.0;
            while (resultSet.next()){
                 balance = resultSet.getDouble(1);

            }
            balance = Math.round(balance*100.0)/100.0;
            return  balance;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0.0;

        }

    }


}
