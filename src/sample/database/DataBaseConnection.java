package sample.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by krysn on 07.05.2017.
 */
public class DataBaseConnection {

  private static DataBaseConnection instance = new DataBaseConnection();
  private Connection connection;

    private DataBaseConnection() {
        connectToDb();
    }

    private  void connectToDb() {
        //create a new connection in constructor
        String instanceConnectionName = "turnkey-energy-152512:europe-west1:pokerdatabase";

        String databaseName = "users";
        String username = "java";
        String password = "java123";


        try {
            connection = DriverManager.getConnection("jdbc:mysql://104.155.92.236/pokerAi", username, password);
            System.out.println("you have connection");

            //Connection connection = DriverManager.getConnection(jdbcUrl, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public static DataBaseConnection getInstance() {
        return instance;
    }
    public  Connection getConnection(){
        return connection;
    }


}
