package sample.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import sample.service.UserService;
import sample.service.ViewService;

import java.sql.SQLException;


/**
 * Created by krysn on 16.03.2017.
 */
public class LoginController extends ViewService {
     @FXML private TextField login;
     @FXML private TextField password;
     @FXML private Label feedback;
     @FXML private AnchorPane loginScene;
     UserService userService = new UserService();


    public void login() throws SQLException, InterruptedException {
        //getting values from textfields
        String l = login.getText();
        String p = password.getText();

        //call userService to check if login is correct
        if(userService.login(l,p)){
            this.openWindow("../views/homeView.fxml",getStage(),600, 600);
        }else{
            feedback.setText("wrong user name or password");
        }
    }
    public void register() {
        this.openWindow("../views/registerView.fxml", getStage(),600, 600);

    }
    public Stage getStage(){
        return (Stage) loginScene.getScene().getWindow();

    }





}
