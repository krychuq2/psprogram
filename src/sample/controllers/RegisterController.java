package sample.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;
import sample.models.User;
import sample.service.UserService;
import sample.service.ViewService;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;

/**
 * Created by krysn on 20.03.2017.
 */
public class RegisterController extends ViewService{
    private @FXML TextField login;
    private @FXML PasswordField password;
    private  @FXML PasswordField repeatPassword;
    private  @FXML Label loginError;
    private  @FXML Label balanceError;
    private  @FXML Label passwordError;
    private  @FXML Button path;
    private @FXML Label pathError;
    private  @FXML TextField balance;
    private @FXML AnchorPane homeView;
    private String pathToDirectory;
    private UserService userService = new UserService();
    private ArrayList<Validate> formFields;


    @FXML
    public void initialize() {
        //create array of textField
        formFields = new ArrayList<>();
        formFields.add(new Validate(login, loginError, 3, "username", false ));
        formFields.add(new Validate(balance, balanceError, 1, "balance", true));

    }

    public void setPath(){
        DirectoryChooser fileChooser = new DirectoryChooser();
        fileChooser.setTitle("Open Resource File");
        File folder = fileChooser.showDialog(null);
        path.setText(folder.getAbsolutePath());
          pathToDirectory = folder.getAbsolutePath();
    }

    public void register() throws SQLException {
        Boolean checkForm = validateForm();
        Boolean validatePassword = validatePassword();
        Boolean validatePath = validatePath();
        System.out.println(balance.getText()+ "this is balance");

        if(checkForm && validatePassword && validatePath){

            User user =  User.valueOf(1,login.getText(), password.getText(), path.getText(),Double.parseDouble(balance.getText()));

            if(userService.registerUser(user)){
                this.openWindow("../views/homeView.fxml", getStage(),600,600);
            }
        }else{
        }


    }
    public Boolean validateForm(){
        Boolean form = true;
        //going through array list of textfields
        for(int i = 0; i<formFields.size(); i++){
            //temp var
           Validate validate = formFields.get(i);
           //checking if filed is not number
           if(!validate.isNumber){
               //check if field is empety
               if(validate.textField.getText().length()==0){
                   validate.label.setText(formFields.get(i).required);
                   form = false;
                //   validate.textField.setText("");
                   // check min lengh
               }else if(validate.textField.getText().length()<validate.minLenght){
                   validate.label.setText(validate.lenghtError);
                   form = false;

                   //  validate.textField.setText("");

               }else{
                   validate.label.setText("");


               }
           }else{
               if(validate.textField.getText().length()==0){
                   validate.label.setText(validate.required);
                   validate.textField.setText("");
                   form = false;

               }else if( StringUtils.isNumeric(validate.textField.getText())){
                   validate.label.setText("");


               }else {
                    validate.label.setText("It has to be number");
                   form = false;

                   validate.textField.setText("");
               }


           }


        }
        return form;
    }

    public class Validate{
        TextField textField;

        public Validate(TextField textField, Label label, int minLenght, String name, Boolean isNumber) {
            this.textField = textField;
            this.label = label;
            this.minLenght = minLenght;
            this.name = name;
            this.lenghtError = name + " should be at least "+minLenght + " long";
            this.required = name + " is required";
            this.isNumber = isNumber;
        }

        Label label;
        String required;
        int minLenght;
        String lenghtError;
        String name;
        Boolean isNumber;

    }
    public Boolean validatePassword(){
        //check if password is empty
        if(password.getText().length()==0){
            passwordError.setText("password is required");
            //check if password is longer than 3 char
        }else if(password.getText().length()<3){
            passwordError.setText("password should be at least 3 long");
            //check if passwrod are same
        }else if(password.getText().equals(repeatPassword.getText())){
            passwordError.setText("");
            return true;
        }else{
            passwordError.setText("password and repeat password field should be identical");
        }
        return false;

    }
    public Boolean validatePath(){
        if(path.getText().equals("choose torunaments location")){
            pathError.setText("tournaments location is required");
            return false;
        }else {
            pathError.setText("");
            return true;
        }

    }
    public Stage getStage(){
        return (Stage) pathError.getScene().getWindow();

    }


}
