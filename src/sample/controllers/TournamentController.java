package sample.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import sample.models.User;
import sample.models.Tournament;
import sample.service.TournamentService;
import sample.service.ViewUpdater;

import javax.jws.soap.SOAPBinding;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by krystian on 12/02/2017.
 */
public class TournamentController implements Initializable{

    //@FXML private ListView<Tournament> tournamentList;
    @FXML private TableView<Tournament> tournamentTable;
    @FXML private TableColumn<Tournament, String> id;
    @FXML private TableColumn<Tournament, Double> cost;
    @FXML private TableColumn<Tournament, Date> date;
    private List<Tournament> tournaments = new ArrayList<>();
    private TournamentService tournamentService = new TournamentService();
    private User user = User.getUser();
    private ViewUpdater viewUpdater = ViewUpdater.getViewUpdater();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
           viewUpdater.setTournamentController(this);
       // tournamentService.setTournamentController(this);

      //  tournamentService.setTournamentTable(tournamentTable);
        // Date date = new Date();
        // list.add(new Tournament("this is id", 1.0, date));
        String userId = Integer.toString(User.getUser().getId());
        tournaments = user.getTournaments();
        System.out.println(tournaments + " this is tournamnets");
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        cost.setCellValueFactory(new PropertyValueFactory<Tournament, Double>("cost"));
        date.setCellValueFactory(new PropertyValueFactory<Tournament, Date>("date"));

        ObservableList<Tournament> observableList = FXCollections.observableList(tournaments);
        tournamentTable.setItems(observableList);

    }

    public void getNotify(){
        ObservableList<Tournament> observableList = FXCollections.observableList(user.getTournaments());
        tournamentTable.setItems(observableList);
        System.out.println("helo helo mf");
    }
}
