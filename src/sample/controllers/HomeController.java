package sample.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.text.TextAlignment;
import sample.fileReaders.FileWatcher;
import sample.models.User;
import sample.service.TournamentService;
import sample.service.UserService;
import sample.service.ViewService;
import sample.service.ViewUpdater;

/**
 * Created by krysn on 23.03.2017.
 */
public class HomeController extends ViewService {

    @FXML Label welcome;
    private User user;
    private @FXML Label tournamentTip;
    TournamentService tournamentService = new TournamentService();
    private UserService userService = new UserService();
    private ViewUpdater viewUpdater = ViewUpdater.getViewUpdater();

    @FXML
    public void initialize() {
        viewUpdater.setHomeController(this);
        //get users
        user = User.getUser();
        welcome.setText("BALANCE" + " " + user.getBalance() +" $");
        double playAmout = user.getBalance()*0.02;
        playAmout = Math.round(playAmout*100.0)/100.0;
        if(playAmout<0){
            tournamentTip.setText("You can't play any tournaments, your balance is " + " " + Math.round(user.getBalance()*100.0)/100.0);
            tournamentTip.setWrapText(true);
            tournamentTip.setTextAlignment(TextAlignment.JUSTIFY);
        }
        else{
            tournamentTip.setText("You can play tournaments which costs " + playAmout + " $ or less");
            tournamentTip.setWrapText(true);
            tournamentTip.setTextAlignment(TextAlignment.JUSTIFY);

        }
        enableWatcher();
        saveTournaments();

    }

    public void deposit(){

        String path = "../views/account/deposit.fxml";
        this.openWindow(path, null, 539, 416 );
    }
    public void withdraw(){
        String path = "../views/account/withdraw.fxml";
        this.openWindow(path, null, 539, 416 );
    }

    public void enableWatcher(){
        FileWatcher pathTest = new FileWatcher();
          new Thread(pathTest).start();
    }
    public void saveTournaments(){
        String userId = Integer.toString(User.getUser().getId());
    //    user.updateBalance();
        user.setTournaments(tournamentService.getAllTournaments(userId));
//        welcome.setText(user.getLogin()+" "+ userService.getBalance(User.getUser().getId()) +" $");
    }
    public void tournaments(){
        String userId = Integer.toString(User.getUser().getId());
        System.out.println("we are trying to get tournaments for: "+userId);
        String path = "../views/tournamentsView.fxml";
        this.openWindow(path, null, 600, 600);
    }
    public void getNotify(){
        double userAmount = userService.getBalance(User.getUser().getId());
        System.out.println("new balance "+ userAmount);
        welcome.setText("balance"+" "+ userAmount +" $");
        double playAmout = userAmount*0.02;
        playAmout = Math.round(playAmout*100.0)/100.0;
        tournamentTip.setText("You can play tournaments which costs " +  + playAmout + " $ or less");

    }

}
