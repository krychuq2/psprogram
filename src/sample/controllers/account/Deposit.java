package sample.controllers.account;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;
import sample.database.AccountOperations;
import sample.models.User;
import sample.service.UserService;
import sample.service.ViewUpdater;

import javax.xml.soap.Text;
import java.io.IOException;

/**
 * Created by krysn on 28.03.2017.
 */
public class Deposit {
 @FXML
    TextField amount;
 @FXML
 Label depositError;
 @FXML
    Button depositButton;
 private UserService userService = new UserService();
 private ViewUpdater viewUpdater = ViewUpdater.getViewUpdater();


 public void deposit() {

     //check if deposit field is not empty
     if (amount.getText().length() == 0) {
         depositError.setText("Deposit amount can not be empty");
     } else {
         //check if we can parse text to double
         try {
             double amountValue = Double.parseDouble(amount.getText());
             if (amountValue <= 0) {
                 depositError.setText("Deposit amount needs to be bigger than 0");
             } else {
                 userService.addDeposit(amountValue, User.getUser().getId());
                 viewUpdater.getNotify();
                 Stage stage = (Stage) amount.getScene().getWindow();
                 // do what you have to do
                 stage.close();
             }
         } catch (NumberFormatException e) {
             depositError.setText("Deposit amount needs to be a number");
             //not a double
         }
     }

 }
}
