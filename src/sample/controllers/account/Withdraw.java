package sample.controllers.account;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.models.User;
import sample.service.UserService;
import sample.service.ViewUpdater;

/**
 * Created by krysn on 13.06.2017.
 */
public class Withdraw {
    @FXML
    TextField amount;
    @FXML Label withdrawError;
    @FXML
    Button withdrawButton;
    private UserService userService = new UserService();
    private ViewUpdater viewUpdater = ViewUpdater.getViewUpdater();


    public void withdraw(){
        //check if deposit field is not empty
        if (amount.getText().length() == 0) {
            withdrawError.setText("Withdraw amount can not be empty");
        } else {
            //check if we can parse text to double
            try {
                double amountValue = Double.parseDouble(amount.getText());
                if (amountValue <= 0) {
                    withdrawError.setText("Withdraw amount needs to be bigger than 0");
                } else {
                    double negativeAmount = -amountValue;
                    userService.addDeposit(negativeAmount, User.getUser().getId());
                    viewUpdater.getNotify();
                    Stage stage = (Stage) amount.getScene().getWindow();
                    // do what you have to do
                    stage.close();
                }
            } catch (NumberFormatException e) {
                withdrawError.setText("Withdraw amount needs to be a number");
                //not a double
            }
        }

    }

}
