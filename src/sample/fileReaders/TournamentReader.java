package sample.fileReaders;


import sample.models.User;
import sample.models.Tournament;
import sample.service.TournamentService;
import sample.service.UserService;

import java.io.File;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by krysn on 28.01.2017.
 */
//"/Users/krystian/Desktop/ps/psprogram/hands/"
public class TournamentReader {
    private List<Tournament> tournaments = new ArrayList<Tournament>();
    private String pathToTournaments = null;
    //BufferedReader bufferedReader = new BufferedReader(new FileReader())
    //create a folder where we are going to search
    private File folder;
    //get list of files from our folder
    private File[] listOfFiles;
    private double totalCost = 0.0;
    private ArrayList<String>handsId = new ArrayList<String>();
    TournamentService tournamentService = new TournamentService();
    String newDirectoryName = null;
    UserService userService = new UserService();
    User user = User.getUser();

    public List<Tournament> getAllTournaments(){

         folder  = new File(userService.getPath());
         newDirectoryName = folder + "\\saved";

        File newDirectory = new File(newDirectoryName);

        if(!newDirectory.exists()){
            newDirectory.mkdir();
        }

        listOfFiles = folder.listFiles();

        if(folder.exists()){
            System.out.println("folder exists");

        }else {
            System.out.println(folder.getName());
            System.out.println(folder.getAbsolutePath());
            System.out.println("doesnt");
        }
        System.out.println(folder.getName());

        //loop throught all files objc
        for (int i = 0; i < listOfFiles.length; i++) {
            //check if file is file
            if (listOfFiles[i].isFile()) {

                listOfFiles[i].renameTo(new File(newDirectoryName + "\\" + listOfFiles[i].getName()));

                //get filename
                String fileName = listOfFiles[i].getName();
                //get date from file name from position
                String date = getDate(fileName);
                //initialise id of tournament
                String id = getTournamentId(fileName);
                //assign id to return obj from method which is getting id
               // totalCost = totalCost + getCost(fileName);
                double cost = getCost(fileName);
                userService.addTournamentCost(-cost, user.getId());
               // HandService handService = new HandService();
                //ArrayList<Hand> hands = handService.getHands(fileName);


                tournaments.add(new Tournament(id,cost,date));

                //update wallet


                //System.out.println(cost);
              //  System.out.println(fileName);

            }
        }

        return tournaments;
    }
    public Tournament createTournament(String fileName){

        //get date from file name from position
        String date = getDate(fileName);
        //initialise id of tournament
        String id = getTournamentId(fileName);
        //assign id to return obj from method which is getting id
        // totalCost = totalCost + getCost(fileName);
        double cost = getCost(fileName);
        // HandService handService = new HandService();
        //ArrayList<Hand> hands = handService.getHands(fileName);


         return new Tournament(id,cost,date);



    }

    public Tournament getNewTournament(){

        String fileName = tournamentService.getNewFileName();
        //get date from file name from position
        String date = getDate(fileName);
        //initialise id of tournament
        String id = getTournamentId(fileName);
        //assign id to return obj from method which is getting id
        // totalCost = totalCost + getCost(fileName);
        double cost = getCost(fileName);
        // HandService handService = new HandService();
        //ArrayList<Hand> hands = handService.getHands(fileName);

        folder  = new File(userService.getPath());


        listOfFiles = folder.listFiles();

        newDirectoryName = folder + "\\saved";

        File newDirectory = new File(newDirectoryName);

        if(!newDirectory.exists()){
            newDirectory.mkdir();
        }

        for (int i = 0; i < listOfFiles.length; i++) {
            //check if file is file
            if (listOfFiles[i].isFile()) {
                listOfFiles[i].renameTo(new File(newDirectoryName + "\\" + listOfFiles[i].getName()));
                System.out.println("moved to saved");
            }
        }
        return (new Tournament(id,cost,date));

    }
    public Boolean moveFiled(){
        System.out.println("we will move new files");
          //creating a folder with new path
          folder  = new File(userService.getPath());
          //getting files from dir
          listOfFiles = folder.listFiles();

          newDirectoryName = folder + "\\saved";
          File newDirectory = new File(newDirectoryName);
         //check if dir exist
          if(!newDirectory.exists()){
             newDirectory.mkdir();
        }

      for (int i = 0; i < listOfFiles.length; i++) {
            //check if file is file
            if (listOfFiles[i].isFile()) {
                listOfFiles[i].renameTo(new File(newDirectoryName + "\\" + listOfFiles[i].getName()));
                System.out.println("moved to saved");
            }
        }
        return true;
    }



    public String getTournamentId(String s){
        String tournamentId = "";
        for( int i = 0; i <  s.length(); i++)
        {
            //go til T in HH20170609 T1937741809 No Limit Hold'em $6.96 + $0.14
            if(s.charAt(i) == 'T'){
                i++;
                while (s.charAt(i) != ' '){
                    tournamentId += s.charAt(i);
                    i++;
                }

            }
        }
        return  tournamentId;
    }

    public String getDate(String s){


        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat format = new SimpleDateFormat("YYYYMMDD");
        DateFormat format1 = new SimpleDateFormat("DD-MM-YYYY");

        Date date = new Date();
        String currentTime=null;
        String dataString = "";
        //  Date date = s.substring(2,10);
        //System.out.println(s.substring(2,10));


            dataString = s.substring(2,10);
              currentTime = sdf.format(date);

         //   System.out.println(date);



        return  dataString;

    }

    public Double getCost(String s){
        char ch = 'a';
        int index = s.indexOf('$');
        int iend = s.indexOf("+");
        double feeInt= 0.0;
        double rekeInt = 0.0;
        String fee = "";
        String rake = "";
        if(index !=-1){
            index++;
            fee = s.substring(index, iend);
             feeInt = Double.parseDouble(fee);

            iend = iend + 3;

            while(ch != 't'){
                ch = s.charAt(iend);
                rake += ch;
                iend++;

            }

            rake = rake.substring(0, rake.length()-2);
            rekeInt = Double.parseDouble(rake);
            double total = rekeInt +  feeInt;
            return total;

        }else{
            return 0.0;
        }



    }
}
