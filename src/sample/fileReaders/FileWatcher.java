package sample.fileReaders;

import javafx.application.Platform;
import sample.controllers.HomeController;
import sample.controllers.TournamentController;
import sample.models.User;
import sample.models.Tournament;
import sample.service.TournamentService;
import sample.service.UserService;
import sample.service.ViewUpdater;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;

/**
 * Created by Palko on 08/04/2017.
 */
public  class FileWatcher implements Runnable {
    public FileWatcher(){

    }
    //public FileWatcher(HomeController homeController){
    //    this.homeController = homeController;
   // }

    private static String newFile = null;
    private ViewUpdater viewUpdater = ViewUpdater.getViewUpdater();

  //  private static HomeController homeController = new HomeController();


    private  void listenForChanges(File file) throws IOException {

        Path path = file.toPath();
        if (file.isDirectory()) {
            WatchService ws = path.getFileSystem().newWatchService();
            path.register(ws, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
            WatchKey watch = null;
            while (true) {
                System.out.println("Watching directory: " + file.getPath());
                try {
                    watch = ws.take();
                } catch (InterruptedException ex) {
                    System.err.println("Interrupted");
                }
                List<WatchEvent<?>> events = watch.pollEvents();
                watch.reset();
                for (WatchEvent<?> event : events) {
                    WatchEvent.Kind<Path> kind = (WatchEvent.Kind<Path>) event.kind();
                    Path context = (Path) event.context();
                    if (kind.equals(StandardWatchEventKinds.OVERFLOW)) {
                        System.out.println("OVERFLOW");
                    } else if (kind.equals(StandardWatchEventKinds.ENTRY_CREATE)) {




                        newFile = String.valueOf(context.getFileName());

//
                        Path path1 = context.getFileName();
                        String fileName = path1.toString();
                        System.out.println("We are going to save " + fileName);

                        System.out.println("Created: " + context.getFileName());

                        if(saveFile(fileName)){
                          //  synchronized (homeController) {
                            Platform.runLater(() -> {
                                viewUpdater.getNotify();

                            });
                            // }

                            System.out.println("tournament saved");
                        }else{
                            System.out.println("tournament didn't save");

                        }

                    } else if (kind.equals(StandardWatchEventKinds.ENTRY_DELETE)) {
                        System.out.println("Deleted: " + context.getFileName());
                    } else if (kind.equals(StandardWatchEventKinds.ENTRY_MODIFY)) {
                        System.out.println("Modified: " + context.getFileName());
                    }
                }
            }
        } else {
            System.err.println("Not a directory. Will exit.");
        }
    }

    public String getNewFile(){
        return newFile;
    }


    private static Boolean saveFile(String fileName){
        TournamentService ts = new TournamentService();
        TournamentReader tr = new TournamentReader();
        Tournament t  = tr.createTournament(fileName);
        return ts.insertNewTournament(t, Integer.toString(User.getUser().getId()));

    }

    @Override
    public void run() {

        UserService userService = new UserService();

        File file = new File(userService.getPath());
        try {
            System.out.println("Listening on: " + file);
            listenForChanges(file);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}