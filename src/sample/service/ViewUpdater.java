package sample.service;

import sample.controllers.HomeController;
import sample.controllers.TournamentController;

/**
 * Created by krysn on 21.05.2017.
 */
public class ViewUpdater {
    static ViewUpdater viewUpdater;
    private HomeController homeController;
    private TournamentController tournamentController;

    public static ViewUpdater getViewUpdater(){
        if(viewUpdater != null){
            return viewUpdater;
        }else{
            viewUpdater = new ViewUpdater();
            return viewUpdater;
        }
    }
    private ViewUpdater(){

    }
    public void getNotify(){
        homeController.getNotify();
        if(tournamentController != null){
            tournamentController.getNotify();
        }
        //System.out.println("view updater has been notify");
    }
    public void setHomeController(HomeController homeController){
        this.homeController = homeController;
    }
    public void setTournamentController(TournamentController tournamentController){
        this.tournamentController = tournamentController;
    }
}

