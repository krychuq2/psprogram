package sample.service;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import sample.controllers.HomeController;
import sample.controllers.TournamentController;
import sample.database.AccountOperations;
import sample.database.TournamentOperations;
import sample.fileReaders.FileWatcher;
import sample.models.User;
import sample.fileReaders.TournamentReader;
import sample.models.Tournament;
import sample.watchers.WalletWatcher;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by krysn on 28.01.2017.
 */
public class TournamentService {
  // static TournamentService tournamentService = new TournamentService();
   private AccountOperations accountOperations = new AccountOperations();
  // HomeController homeController = HomeController.getHomeController();
   static TableView<Tournament> tournamentTable;
   static Label welcome;
   static Label tournamentTrip;
   private UserService userService = new UserService();
   WalletWatcher walletWatcher = new WalletWatcher();

   User user = User.getUser();
   FileWatcher pathTest;


   public TournamentService(){

   }

    TournamentOperations tournamentOperations = new TournamentOperations();
   // FileWatcher pathTest = new FileWatcher();
    List<Tournament> tournaments = new ArrayList<>();

    public Tournament getNewTournament(){
        TournamentReader tournamentReader = new TournamentReader();

        return  tournamentReader.getNewTournament();

    }

    public String getChangedFile() {
        return pathTest.getNewFile();
    }

//    public String getUserId() {
//
//        return tournamentOperations.getUserId();
//    }

    public List<Tournament> getAllTournaments(String userId) {
        //creating torunamet reader obj
        TournamentReader tournamentReader = new TournamentReader();

        try {
            //get tournaments form reader and insert them to db
            if(insertTournament(tournamentReader.getAllTournaments(), userId)){
                //get tournaments from database
                tournaments = getTournament(userId);

               // walletWatcher.updateWallet();

                return tournaments;
            }else {
                  return null;

            }
        } catch (SQLException e) {
            e.printStackTrace();
           return null;
        }
    }

    public Boolean insertTournament(List<Tournament> tournaments, String userId) throws SQLException {

        for (Tournament tournament : tournaments) {
            //saving tournemnt in the loop
            System.out.println("we are going to save torunament");
            tournamentOperations.insertTournaments(tournament, userId);
        }
        return true;
    }

    public String getNewFileName(){

        String changedFile = pathTest.getNewFile();

        return changedFile;
    }



    public Boolean insertNewTournament(Tournament tournament,String userId){
        try {
            tournamentOperations.insertTournaments(tournament,userId);
            TournamentReader tournamentReader = new TournamentReader();
            tournamentReader.moveFiled();
            tournaments.add(tournament);
            user.addTournament(tournament);

            ObservableList<Tournament> observableList = FXCollections.observableList(tournaments);
//            tournamentTable.setItems(observableList);
            double cost = tournament.getCost();
            userService.addTournamentCost(-cost, user.getId());
           // System.out.println("we will try to update fck wallet");


           // updateWallet();
            //walletWatcher.updateWallet();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


    public List<Tournament> getTournament(String userId){
       tournaments = tournamentOperations.getTournaments(userId);
        return tournaments;
    }
    public List<Tournament> getTournaments(){
        return tournaments;
    }



}
