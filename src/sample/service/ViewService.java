package sample.service;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by krysn on 26.04.2017.
 */
public class ViewService {

    public ViewService(){

    }

    public boolean openWindow(String path, Stage previousStage, int width, int height){
        //open new stage
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource(path));
            stage.setScene(new Scene(root, width, height));
            stage.setTitle("Welcome");
            stage.show();
            if(previousStage!=null){
                previousStage.close();
            }
            return true;

        } catch (IOException e) {
            System.out.println("error: ---->"+e);
            e.printStackTrace();
            return false;
        }
    }

}
