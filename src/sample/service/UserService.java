package sample.service;

import sample.database.AccountOperations;
import sample.database.TournamentOperations;
import sample.database.UsersOperations;
import sample.models.User;


/**
 * Created by krysn on 17.03.2017.
 */
public class UserService {
    private TournamentOperations tournamentOperations = new TournamentOperations();
    private AccountOperations accountOperations = new AccountOperations();
    private UsersOperations usersOperations = new UsersOperations();

    public Boolean registerUser(User user){
      return  usersOperations.registerUser(user);
    }
    public Boolean checkLogin(String login){
        return usersOperations.checkLogin(login);
   }
    public Boolean updateWallet(int amount){return  accountOperations.deposit(amount);}
    public Boolean addTournamentCost(Double amount, int userId){ return tournamentOperations.addTournamentCost(amount, userId);}
    public Boolean addDeposit(Double amount, int userId){return tournamentOperations.addTournamentCost(amount, userId);}
    public Double getBalance(int userId){
      return  accountOperations.getBalance(userId);
    }
    public Boolean login(String login, String password){

        return usersOperations.login(login, password);

    }
    public User getUser(){
        return User.getUser();
    }
    public String getPath(){return User.getUser().getPath();}



 //   public User getUser(String login, String password){

//        return tournamentOperations.getUser(login, password);
    //}

}
